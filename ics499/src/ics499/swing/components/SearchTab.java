package ics499.swing.components;

import ics499.TwitterManager;
import ics499.swing.ApplicationWindow;
import ics499.swing.tablemodel.ListTableModel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import twitter4j.QueryResult;
import twitter4j.Tweet;

/**
 * Represents the Applications' Search Tab.  Note that this class does not extend JPanel,
 * but uses composition over inheritance to represent a tab.  A JPanel instance is
 * is accessed and returned via the getTab() method.  Also note that the instance is 
 * first wrapped in a Twitter Tab returned from the TabFactory.  Each tab in the 
 * Application should do the same.
 * 
 * @author Felix Maina
 */
public class SearchTab implements ActionListener {

	private JPanel tab;
	JButton enterKey;
	JTextField textBox;
	String searchQuery;
	JScrollPane tableScrollPane;
	QueryResult result = null;
	JTable usersTable;
	

	TwitterManager tsearch = new TwitterManager();
	ApplicationWindow wind = new ApplicationWindow();
	private Map<String, Tweet> searchCache = new HashMap<String, Tweet>();

	public void populateSearch() {
		searchQuery = textBox.getText();
		result = tsearch.queryResult(searchQuery);
		populateModel(result);

	}
	
	public void populateFromTrend(String searchQuery){			
		textBox.setText(" ");
		textBox.setText(searchQuery);
		result = tsearch.queryResult(searchQuery);
		populateModel(result);		
	}
	
	public void setsearchText(){
		
	}
	
	
	public JTable getUsersTable() {
		
		return usersTable;
	}

	public JPanel getTab() {
		// Creation of tab happens here, instead of at construction to prevent
		// eager instantiation when Spring initializes the Application

		if (this.tab != null) {
			return this.tab;
		}

		enterKey = new JButton("Enter");
		enterKey.setIcon(new ImageIcon(ApplicationWindow.class
				.getResource("images/search.png")));
		enterKey.addActionListener(this);

		textBox = new JTextField("Enter the keyword to search");
		textBox.setForeground(Color.blue);
		
		
		// Get Tweets to initialize JTable
		result = tsearch.queryResult("USA");

		int count = 1;

		// Create 2-D List for Table Data Model
		@SuppressWarnings("rawtypes")
		ArrayList<List> rows = new ArrayList<List>();

		if (result != null) {
			for (Tweet tweet : result.getTweets()) {
				searchCache.put(tweet.getFromUser(), tweet);

				ArrayList<Object> entityData = new ArrayList<Object>();
				entityData.add(count);
				entityData.add(tweet.getFromUser());
				entityData.add(tweet.getText());

				rows.add(entityData);
				count++;
			}
		}

		// List of column names to pass to Table Model
		ArrayList<String> columnNames = new ArrayList<String>();
		columnNames.add("Count");
		columnNames.add("User name");
		columnNames.add("User Tweet");

		// Create Users Table
		usersTable = new JTable() {
					
				
				private static final long serialVersionUID = -1698365488319779858L;

					@Override
				public boolean isCellEditable(int rowIndex, int colIndex) {
					
						return false;
				}
		};
		usersTable.setFont(new Font("SansSerif", Font.PLAIN, 13));
		usersTable.setRowHeight(23);
		usersTable.setShowGrid(false);
		usersTable.setShowVerticalLines(true);
		usersTable.setShowHorizontalLines(true);

		// Set Table Model using Tweet data and column names
		usersTable.setModel(new ListTableModel(rows, columnNames));

		// Set column preferences
		usersTable.getColumnModel().getColumn(0).setPreferredWidth(60);
		usersTable.getColumnModel().getColumn(0).setMaxWidth(60);
		usersTable.getColumnModel().getColumn(1).setPreferredWidth(150);
		usersTable.getColumnModel().getColumn(1).setMaxWidth(150);

		// Put Table and TextArea in a ScrollPane
		tableScrollPane = new JScrollPane(usersTable);
		
		// Get a TwitterTab instance with our configured scrollpanes
		return this.tab = new TabFactory().newSearchTab(tableScrollPane,
				enterKey, textBox);

	}

	/**
	 *  Populates TableModel with data
	 *  
	 * @param tweets a list of Tweets to use as data
	 */
	private void populateModel(QueryResult result) {

		if (result == null) {
			return;
		}

		int count = 1;
		searchCache.clear();

		// Create 2-D List for Table Data Model
		@SuppressWarnings("rawtypes")
		ArrayList<List> rows = new ArrayList<List>();
		for (Tweet tweet : result.getTweets()) {
			searchCache.put(tweet.getFromUser(), tweet);

			ArrayList<Object> entityData = new ArrayList<Object>();
			entityData.add(count);
			entityData.add(tweet.getFromUser());
			entityData.add(tweet.getText());

			rows.add(entityData);
			count++;
		}

		ListTableModel model = (ListTableModel) usersTable.getModel();

		if (model.getRowCount() > 0) {
			model.removeRowRange(0, model.getRowCount() - 1);
		}

		model.insertRows(0, rows);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		populateSearch();
	}

}
