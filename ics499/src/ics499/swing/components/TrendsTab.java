package ics499.swing.components;

import ics499.TwitterManager;
import ics499.swing.ApplicationWindow;
import ics499.swing.tablemodel.ListTableModel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import twitter4j.Trend;
import twitter4j.Trends;


@SuppressWarnings("rawtypes")
public class TrendsTab implements ActionListener{

	TwitterManager tweetTrends = new TwitterManager();
	private Map<String, Trend> tableOneCache = new HashMap<String, Trend>();
	private Map<String, Trend> tableTwoCache = new HashMap<String, Trend>();

	private JPanel tab;
	JButton refreshKey;
	JTable usersTable1;
	JTable usersTable;
	
	public JTable getUsersTable() {
		
		return usersTable;
	}
	
	public JTable getUsersTableOne() {
		
		return usersTable1;
	}
	
	private void performRefresh() {
		List<Trends> tweets = tweetTrends.weeklyTrends();
		List<Trends> tweets1 = tweetTrends.dailyTrends();
		refreshTablesOne(tweets);
		refreshTablesTwo(tweets1);
	}

	public JPanel getTab() {
		// Creation of tab happens here, instead of at construction to prevent
		// eager instantiation when Spring initializes the Application

		if (this.tab != null) {
			return this.tab;
		}
		
		refreshKey = new JButton(" REFRESH");
		refreshKey.setIcon(new ImageIcon(ApplicationWindow.class
				.getResource("images/refresh.png")));
		refreshKey.addActionListener(this);


		// Get Weekly trending tweets
		List<Trends> tweets = tweetTrends.weeklyTrends();

		// Create 2-D List for Table Data Model
		ArrayList<List> rows = new ArrayList<List>();

		if (tweets != null) {
			for (Trends t : tweets) {
				for (Trend trend : t.getTrends()) {
					tableOneCache.put(trend.getName(), trend);
					ArrayList<Object> entityData = new ArrayList<Object>();
					entityData.add(trend.getName());
					rows.add(entityData);
				}
				break;
			}
		}

		// List of column names to pass to Table Model
		ArrayList<String> columnNames = new ArrayList<String>();
		columnNames.add("TRENDING THIS WEEK");

		usersTable1 = new JTable(){				
			
			private static final long serialVersionUID = 6107702324323989523L;			

			@Override
			public boolean isCellEditable(int rowIndex, int colIndex) {
			
				return false;
			}
		};
		usersTable1.setFont(new Font("SansSerif", Font.PLAIN, 14));
		usersTable1.setRowHeight(23);
		usersTable1.setShowGrid(false);
		usersTable1.setShowVerticalLines(true);
		usersTable1.setShowHorizontalLines(true);

		// Set Table Model using Tweet data and column names
		usersTable1.setModel(new ListTableModel(rows, columnNames));

		// Get daily trending tweets
		List<Trends> tweets1 = tweetTrends.dailyTrends();

		// Create 2-D List for Table Data Model
		ArrayList<List> rows1 = new ArrayList<List>();

		if (tweets1 != null) {
			for (Trends t : tweets1) {
				for (Trend trend : t.getTrends()) {
					tableTwoCache.put(trend.getName(), trend);
					ArrayList<Object> entityData = new ArrayList<Object>();
					entityData.add(trend.getName());
					rows1.add(entityData);
				}
				break;
			}
		}

		// List of column names to pass to Table Model
		ArrayList<String> columnNames1 = new ArrayList<String>();
		columnNames1.add("TRENDING TODAY");

		// Create Tweets display Table
		usersTable = new JTable(){				
			
			private static final long serialVersionUID = -1391701631860917829L;
			
			@Override
			public boolean isCellEditable(int rowIndex, int colIndex) {
			
				return false;
			}
		};
		usersTable.setFont(new Font("SansSerif", Font.PLAIN, 14));
		usersTable.setRowHeight(23);
		usersTable.setShowGrid(false);
		usersTable.setShowVerticalLines(true);
		usersTable.setShowHorizontalLines(true);

		// Set Table Model using Tweet data and column names
		usersTable.setModel(new ListTableModel(rows1, columnNames1));

		// Put Table and TextArea in a ScrollPane
		JScrollPane tableScrollPane = new JScrollPane(usersTable1);
		JScrollPane textScrollPane = new JScrollPane(usersTable);

		// Get a TwitterTab instance with our configured scrollpanes
		this.tab = new TabFactory().newTrendsTab(
				tableScrollPane, textScrollPane,refreshKey);

		return this.tab;
	}
	
	/**
	 *  Populates TableModel with data
	 *  
	 * @param tweets a list of Tweets to use as data
	 */
	private void refreshTablesOne(List<Trends> tweets1) {

		if (tweets1 == null) {
			return;
		}		
				
		tableOneCache.clear();

		// Create 2-D List for Table Data Model
				ArrayList<List> rows = new ArrayList<List>();

				if (tweets1 != null) {
					for (Trends t : tweets1) {
						for (Trend trend : t.getTrends()) {
							tableOneCache.put(trend.getName(), trend);
							ArrayList<Object> entityData = new ArrayList<Object>();
							entityData.add(trend.getName());
							rows.add(entityData);
						}
						break;
					}
				}

		ListTableModel model = (ListTableModel) usersTable1.getModel();

		if (model.getRowCount() > 0) {
			model.removeRowRange(0, model.getRowCount() - 1);
		}

		model.insertRows(0, rows);
		
		
	}
	
	private void refreshTablesTwo(List<Trends> tweets2) {

		
		if(tweets2 == null){
			return;
		}		
		
		tableTwoCache.clear();
		
		// Create 2-D List for Table Data Model
				ArrayList<List> rows = new ArrayList<List>();

				if (tweets2 != null) {
					for (Trends t : tweets2) {
						for (Trend trend : t.getTrends()) {
							tableTwoCache.put(trend.getName(), trend);
							ArrayList<Object> entityData = new ArrayList<Object>();
							entityData.add(trend.getName());
							rows.add(entityData);
						}
						break;
					}
				}
				
				ListTableModel model = (ListTableModel) usersTable.getModel();

				if (model.getRowCount() > 0) {
					model.removeRowRange(0, model.getRowCount() - 1);
				}

				model.insertRows(0, rows);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		performRefresh();
		
	}
	

}
