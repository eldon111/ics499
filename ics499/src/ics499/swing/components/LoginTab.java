package ics499.swing.components;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 * Represents the Applications' Login Tab.  Note that this class does not extend JPanel,
 * but uses composition over inheritance to represent a tab.  A JPanel instance is
 * is accessed and returned via the getTab() method.  Also note that the instance is 
 * first wrapped in a Twitter Tab returned from the TabFactory.  Each tab in the 
 * Application should do the same.
 * 
 * @author Felix Maina
 */
public class LoginTab {

	Border raisedbevel = BorderFactory.createRaisedBevelBorder();
	Border loweredbevel = BorderFactory.createLoweredBevelBorder();
	Border compound = BorderFactory.createCompoundBorder(raisedbevel, loweredbevel);
	private JPanel tab;
	

	public JPanel getTab() {
		// Creation of tab happens here, instead of at construction to prevent
		// eager instantiation when Spring initializes the Application

		if (this.tab != null) {
			return this.tab;
		}
		
		

		JLabel label = new JLabel("Research, Explore & Discover Twitter!", JLabel.CENTER);
			label.setFont(new Font("Book Antiqua", Font.ITALIC, 42));			
			label.setForeground(Color.BLUE);
			label.setBorder(compound);
		    
		
		
		// Get a TwitterTab instance 
		return this.tab = new TabFactory().newLoginTab (label);
		

	}
	
}
