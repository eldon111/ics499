package ics499.swing.components;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

public class SearchTabTableMenu extends JPopupMenu implements ActionListener,
		MouseListener {

	private static final long serialVersionUID = -7830832115255807264L;

	JMenuItem searchTrackUser;
	JMenuItem searchUntrackUser;
	JTabbedPane pane;
	Component component;
	SearchTab searchTab;
	StreamingTab streamingTab;


	public SearchTabTableMenu(SearchTab searchTab) {

		this.searchTab = searchTab;

		// add menu items
		searchTrackUser = new JMenuItem("Track This User");
		searchUntrackUser = new JMenuItem("Untrack This User");

		add(searchTrackUser);
		add(searchUntrackUser);

		// add listeners to each menu item
		for (Component item : this.getComponents()) {
			if (item instanceof JMenuItem) {
				((JMenuItem) item).addActionListener(this);
			}
		}
	}

	public void setStreamingTab(StreamingTab streamingTab) {

		this.streamingTab = streamingTab;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// find tabbed pane ancestor (if one exists)
		if (pane == null) {
			component = searchTab.getTab();
			while (component.getParent() != null) {
				if (component.getParent() instanceof JTabbedPane) {
					pane = (JTabbedPane) component.getParent();
					break;
				}
				component = component.getParent();
			}
		}

		// User clicked 'Track/Untrack User'
		if (e.getSource() == searchTrackUser
				|| e.getSource() == searchUntrackUser) {
			boolean isTracked = false;
			// get user name
			int row = searchTab.getUsersTable().getSelectedRow();
			int col = 1; // User name column
			String name = (String) searchTab.getUsersTable().getModel()
					.getValueAt(row, col);

			if (e.getSource() == searchTrackUser) {
				isTracked = true;
			}
			if (e.getSource() == searchUntrackUser) {
				isTracked = false;
			}

			streamingTab.saveUserInfo(name, isTracked);
		}
	}


	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			JTable table = null;

			if (e.getSource() == searchTab.getUsersTable()) {
				table = searchTab.getUsersTable();
			}


			int row = table.rowAtPoint(e.getPoint());

			if (table != null && row >= 0) {
				table.setRowSelectionInterval(row, row);
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

}
