package ics499.swing.components;

import ics499.filtering.TweetFilterer;
import ics499.filtering.UserFilter;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

public class TweetsTableMenu extends JPopupMenu implements ActionListener,
		MouseListener {
	
	private static final long serialVersionUID = -7481556435520512865L;
	
	JMenuItem mViewTweets;
	JMenuItem mTrackUser;
	JMenuItem mUntrackUser;
	JMenuItem mResetView;
	
	JTabbedPane pane;
	Component component;
	
	UsersTab usersTab;
	
	TweetsTab tweetsTab;
	StreamingTab streamingTab;
	
	boolean pressed = false;
	
	public TweetsTableMenu(TweetsTab tweetsTab) {
	
		this.tweetsTab = tweetsTab;
		
		// add menu items
		mViewTweets = new JMenuItem("View Tweets From This User");
		mTrackUser = new JMenuItem("Track This User");
		mUntrackUser = new JMenuItem("Untrack This User");
		mResetView = new JMenuItem("Reset View");
		
		add(mViewTweets);
		add(mTrackUser);
		add(mUntrackUser);
		add(new JSeparator());
		add(mResetView);
		
		// add listeners to each menu item
		for (Component item : this.getComponents()) {
			if (item instanceof JMenuItem) {
				((JMenuItem) item).addActionListener(this);
			}
		}
	}
	
	public void setUsersTab(UsersTab usersTab) {
	
		this.usersTab = usersTab;
	}
	
	public void setStreamingTab(StreamingTab streamingTab) {
	
		this.streamingTab = streamingTab;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	
		// find tabbed pane ancestor (if one exists)
		if (pane == null) {
			component = usersTab.getTab();
			while (component.getParent() != null) {
				if (component.getParent() instanceof JTabbedPane) {
					pane = (JTabbedPane) component.getParent();
					break;
				}
				component = component.getParent();
			}
		}
		
		// User clicked 'View Users's Tweets'
		if (e.getSource() == mViewTweets) {
			
			// get Filterer ready with userId
			TweetFilterer tf = tweetsTab.getTweetFilterer();
			tf.clearFilters();
			int row = tweetsTab.getUsersTable().getSelectedRow();
			int col = 2; // Username column
			String username = (String) tweetsTab.getUsersTable().getModel()
					.getValueAt(row, col);
			tf.addFilter(new UserFilter(tf.getUserId(username)));
			
			// show user's tweets
			tweetsTab.refreshTable();
		}
		
		// User clicked 'Track/Untrack User'
		if (e.getSource() == mTrackUser || e.getSource() == mUntrackUser) {
			
			// get user name
			int row = tweetsTab.getUsersTable().getSelectedRow();
			int col = 2; // Username column
			String name = (String) tweetsTab.getUsersTable().getModel()
					.getValueAt(row, col);
			
			boolean isTracked = tweetsTab.getTweetFilterer()
					.isUserTracked(name);
			
			// change user's tracked status
			streamingTab.saveUserInfo(name, !isTracked);
			
			// update users table view
			usersTab.refreshTable();
		}
		
		if (e.getSource() == mResetView) {
			tweetsTab.getTweetFilterer().clearFilters();
			tweetsTab.setPage(0);
			tweetsTab.refreshTable();
		}
	}
	
	private void processClick(MouseEvent e) {
	
		// if (e.isPopupTrigger()) {
		if (SwingUtilities.isRightMouseButton(e) && pressed) {
			
			pressed = false;
			
			JTable usersTable = tweetsTab.getUsersTable();
			int row = usersTable.rowAtPoint(e.getPoint());
			int col = 2; // Username column
			String name = (String) tweetsTab.getUsersTable().getModel()
					.getValueAt(row, col);
			if (row >= 0) {
				usersTable.setRowSelectionInterval(row, row);
			}
			boolean isTracked = tweetsTab.getTweetFilterer()
					.isUserTracked(name);
			
			if (isTracked) {
				mUntrackUser.setVisible(true);
				mTrackUser.setVisible(false);
			} else {
				mUntrackUser.setVisible(false);
				mTrackUser.setVisible(true);
			}
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
	
		pressed = true;
		processClick(e);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
	
		pressed = false;
		processClick(e);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
	
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	
	}
}