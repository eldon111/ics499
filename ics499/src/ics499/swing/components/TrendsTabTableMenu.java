package ics499.swing.components;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

public class TrendsTabTableMenu extends JPopupMenu implements ActionListener,
		MouseListener {

	private static final long serialVersionUID = 7094984816026872428L;

	JMenuItem searchTrackTrend;
	JMenuItem searchUntrackTrend;
	JMenuItem searchTrend;
	JTabbedPane pane;
	Component component;
	TrendsTab trendTab;
	StreamingTab streamingTab;
	SearchTab searchTab;

	public TrendsTabTableMenu(TrendsTab trendTab) {

		this.trendTab = trendTab;

		// add menu items
		searchTrackTrend = new JMenuItem("Track This Trend");
		searchUntrackTrend = new JMenuItem("Untrack This Trend");
		searchTrend = new JMenuItem("Search this Trend");

		add(searchTrend);
		add(searchTrackTrend);
		add(searchUntrackTrend);
		

		// add listeners to each menu item
		for (Component item : this.getComponents()) {
			if (item instanceof JMenuItem) {
				((JMenuItem) item).addActionListener(this);
			}
		}
	}

	public void setStreamingTab(StreamingTab streamingTab) {

		this.streamingTab = streamingTab;
	}
	
	public void setSearchTab(SearchTab searchTab) {

		this.searchTab = searchTab;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// find tabbed pane ancestor (if one exists)
		if (pane == null) {
			component = trendTab.getTab();
			while (component.getParent() != null) {
				if (component.getParent() instanceof JTabbedPane) {
					pane = (JTabbedPane) component.getParent();
					break;
				}
				component = component.getParent();
			}
		}
		
		if(e.getSource() == searchTrend){
			
			String name = null;
			int row = -1;
			int col = 0; // User name column
			// get user name
			if (row == -1) {
				row = trendTab.getUsersTable().getSelectedRow();
				if (row == -1) {
					row = trendTab.getUsersTableOne().getSelectedRow();
					name = (String) trendTab.getUsersTableOne().getModel()
							.getValueAt(row, col);
				}
				if (trendTab.getUsersTable().getSelectedRow() != -1) {
					name = (String) trendTab.getUsersTable().getModel()
							.getValueAt(row, col);
				}
			}
			
			// switch to Tweets Tab (JPanel)
			pane.setSelectedComponent(searchTab.getTab());			
			searchTab.populateFromTrend(name);			
		}

		// User clicked 'Track/Untrack Trends'
		if (e.getSource() == searchTrackTrend
				|| e.getSource() == searchUntrackTrend) {
			boolean isTracked = false;
			String name = null;
			int row = -1;
			int col = 0; // User name column
			// get user name
			if (row == -1) {
				row = trendTab.getUsersTable().getSelectedRow();
				if (row == -1) {
					row = trendTab.getUsersTableOne().getSelectedRow();
					name = (String) trendTab.getUsersTableOne().getModel()
							.getValueAt(row, col);
				}
				if (trendTab.getUsersTable().getSelectedRow() != -1) {
					name = (String) trendTab.getUsersTable().getModel()
							.getValueAt(row, col);
				}
			}

			if (e.getSource() == searchTrackTrend) {
				isTracked = true;
			}
			if (e.getSource() == searchUntrackTrend) {
				isTracked = false;
			}

			streamingTab.saveKeywordInfo(name, isTracked);
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {

		if (SwingUtilities.isRightMouseButton(e)) {
			JTable table = null;

			if (e.getSource() == trendTab.getUsersTableOne()) {
				table = trendTab.getUsersTableOne();
			} else if (e.getSource() == trendTab.getUsersTable()) {
				table = trendTab.getUsersTable();
			}

			int row = table.rowAtPoint(e.getPoint());

			if (table != null && row >= 0) {
				table.setRowSelectionInterval(row, row);
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

}
