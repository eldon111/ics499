package ics499.swing.components;

import ics499.dataaccess.ImageHandler;
import ics499.dataaccess.TweetDAO;
import ics499.dataaccess.UserEntity;
import ics499.export.TwitterFileWriter;
import ics499.swing.tablemodel.ListTableModel;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Represents the Applications' Tweets Tab. Note that this class does not extend
 * JPanel, but uses composition over inheritance to represent a tab. A JPanel
 * instance is is accessed and returned via the getTab() method. Also note that
 * the instance is first wrapped in a Twitter Tab returned from the TabFactory.
 * Each tab in the Application should do the same.
 * 
 * @author Ryan
 */
public class UsersTab implements ActionListener, ListSelectionListener {
	
	private JPanel tab;
	
	private JTable usersTable;
	
	private Image userImage;
	
	private JFileChooser fc;
	
	
	@SuppressWarnings("serial")
	private JTextArea userTextArea = new JTextArea() {
		
		public void paintComponent(Graphics g) {
		
			super.paintComponent(g);
			if (userImage != null) {
				g.drawImage(userImage, 8, 8, this);
			}
		}
	};
	
	SimpleDateFormat digitFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	private Map<Long, UserEntity> userCache = new HashMap<Long, UserEntity>();
	
	private TweetDAO tweetDAO;

	private int firstResult = 1;
	private int previousFirstResult = -1;	
	private static final int PAGING_INCREMENT = 50;
	private JLabel pageLabel;
	
	public JTable getUsersTable() {
	
		return usersTable;
	}
	
	public JPanel getTab() {
	
		// Creation of tab happens here, instead of at construction to prevent
		// eager instantiation when Spring initializes the Application
		
		if (this.tab != null) {
			return this.tab;
		}
		
		// Get Tweets to initialize JTable
		List<UserEntity> users = tweetDAO.getUsers(0, 50);
		
		// Create 2-D List for Table Data Model
		@SuppressWarnings("rawtypes")
		ArrayList<List> rows = new ArrayList<List>();
		for (UserEntity u : users) {
			userCache.put(u.getUserId(), u);
			
			ArrayList<Object> entityData = new ArrayList<Object>();
			entityData.add(u.getProfileImageURL());
			entityData.add(u.getUserId());
			entityData.add(u.getUsername());
			entityData.add(u.getRealName());
			entityData.add(u.getDescription());
			entityData.add(u.getIsTracked());
			
			rows.add(entityData);
		}
		
		// List of column names to pass to Table Model
		ArrayList<String> columnNames = new ArrayList<String>();
		columnNames.add("Profile Image");
		columnNames.add("User ID");
		columnNames.add("User name");
		columnNames.add("Real Name");
		columnNames.add("Description");
		columnNames.add("IsTracked");
		
		// Create Tweets Table
		usersTable = new JTable() {
			
			private static final long serialVersionUID = -4246361171052172021L;
			
			@Override
			public boolean isCellEditable(int rowIndex, int colIndex) {
			
				return false;
			}
		};
		
		usersTable.setFont(new Font("SansSerif", Font.PLAIN, 13));
		usersTable.setRowHeight(23);
		usersTable.setShowGrid(false);
		usersTable.setShowVerticalLines(true);
		usersTable.setShowHorizontalLines(true);
		usersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		usersTable.getSelectionModel().addListSelectionListener(this);
		
		// Set Table Model using Tweet data and column names
		usersTable.setModel(new ListTableModel(rows, columnNames));
		
		// Set column preferences
		// usersTable.getColumnModel().getColumn(1).setPreferredWidth(150);
		// usersTable.getColumnModel().getColumn(1).setMaxWidth(150);
		usersTable.getColumnModel().getColumn(2).setPreferredWidth(150);
		usersTable.getColumnModel().getColumn(2).setMaxWidth(150);
		usersTable.getColumnModel().getColumn(3).setPreferredWidth(150);
		usersTable.getColumnModel().getColumn(3).setMaxWidth(150);
		usersTable.getColumnModel().getColumn(4).setPreferredWidth(250);
		// usersTable.getColumnModel().getColumn(4).setMaxWidth(400);
		usersTable.getColumnModel().getColumn(5).setPreferredWidth(100);
		usersTable.getColumnModel().getColumn(5).setMaxWidth(100);
		
		// Image URL and UserId is hidden from view, but stays in the model
		usersTable.getColumnModel().removeColumn(
				usersTable.getColumnModel().getColumn(0));
		usersTable.getColumnModel().removeColumn(
				usersTable.getColumnModel().getColumn(0));
		
		userTextArea.setWrapStyleWord(true);
		userTextArea.setLineWrap(true);
		userTextArea.setFont(new Font("SansSerif", Font.PLAIN, 16));
		userTextArea.setEditable(false);
		userTextArea.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		
		// Initialize TextArea to show individual Tweet Info
		if (rows.size() != 0) {
			usersTable.changeSelection(0, 0, false, false);
		} else {
			userTextArea = new JTextArea("No Data");
		}
		
		// Put Table and TextArea in a ScrollPane
		JScrollPane tableScrollPane = new JScrollPane(usersTable);
		tableScrollPane.setPreferredSize(new Dimension(775, 100));
		JScrollPane textScrollPane = new JScrollPane(userTextArea);
		
		// Get a TwitterTab instance with our configured scrollpanes
		this.tab = new TabFactory().newUserTab(TabFactory.HORIZONTAL,
				tableScrollPane, textScrollPane, .7);
		
		// Add Listeners to tab's buttons (must traverse the component tree)
		Component[] tabComponents = tab.getComponents();
		for (int i = 0; i < tabComponents.length; i++) {
			if (tabComponents[i] instanceof Box) {
				
				Component[] boxComponents = ((Box) tabComponents[i])
						.getComponents();
				for (int j = 0; j < boxComponents.length; j++) {
					if (boxComponents[j] instanceof JButton) {
						
						((JButton) boxComponents[j]).addActionListener(this);
					} else if (boxComponents[j] instanceof JLabel) {
						// Add Listener to JLabel to change text
						this.pageLabel = (JLabel) boxComponents[j];
						String pageLabelText = this.formatPageLabelText();
						this.pageLabel.setText(pageLabelText);
					}
				}
			}
		}
		
		// initialize this here, so it will have correct look and feel
		fc = new JFileChooser();
		
		return this.tab;
		
	}
	
	public void refreshTable() {
	
		List<UserEntity> users;
		users = tweetDAO.getUsers(0, 50);
		populateModel(users);
	}
	
	private void populateModel(List<UserEntity> users) {
	
		userCache.clear();
		
		@SuppressWarnings("rawtypes")
		ArrayList<List> rows = new ArrayList<List>();
		for (UserEntity u : users) {
			userCache.put(u.getUserId(), u);
			
			ArrayList<Object> entityData = new ArrayList<Object>();
			entityData.add(u.getProfileImageURL());
			entityData.add(u.getUserId());
			entityData.add(u.getUsername());
			entityData.add(u.getRealName());
			entityData.add(u.getDescription());
			entityData.add(u.getIsTracked());
			
			rows.add(entityData);
		}
		
		ListTableModel model = (ListTableModel) usersTable.getModel();
		
		if (model.getRowCount() > 0) {
			model.removeRowRange(0, model.getRowCount() - 1);
		}
		
		model.insertRows(0, rows);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
	
		if (event.getSource() instanceof JButton) {
			JButton button = (JButton) event.getSource();
			
			if (TabFactory.PREVIOUS_TEXT.equals(button.getText())) {
				// Get last 50 results from TweetDAO
				// Update JLabel to say correct result set
				
				this.firstResult = this.firstResult - PAGING_INCREMENT;
				
				if (this.firstResult < 1) {
					firstResult = 1;
				}
				
				List<UserEntity> users = tweetDAO.getUsers(
						this.firstResult - 1, PAGING_INCREMENT);
				populateModel(users);
				String pageLabelText = this.formatPageLabelText();
				this.pageLabel.setText(pageLabelText);
				
			} else if (TabFactory.NEXT_TEXT.equals(button.getText())) {
				// Get next 50 results from TweetDAO
				// Update JLabel to say correct result set
				
				this.firstResult = this.firstResult + PAGING_INCREMENT;
				
				List<UserEntity> users = tweetDAO.getUsers(
						this.firstResult - 1, PAGING_INCREMENT);
				if (users.size() == 0) {
					this.firstResult = this.firstResult - PAGING_INCREMENT;
					return;
				}
				
				populateModel(users);
				String pageLabelText = this.formatPageLabelText();
				this.pageLabel.setText(pageLabelText);
				
			} else if (TabFactory.EXPORT_TEXT.equals(button.getText())) {
				FileNameExtensionFilter ff = new FileNameExtensionFilter(
						"CSV Files", "csv");
				fc.addChoosableFileFilter(ff);
				fc.setFileFilter(ff);
				fc.setDialogType(JFileChooser.SAVE_DIALOG);
				fc.setMultiSelectionEnabled(false);
				fc.setDialogTitle("Save");
				int returnVal = fc.showSaveDialog(getTab());
				
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					
					String path = fc.getSelectedFile().getAbsolutePath();
					if (!path.matches(".*\\.csv$")) {
						path += ".csv";
					}
					
					File file = new File(path);
					if (file.exists()) {
						// overwriteDialog.sho
						returnVal = JOptionPane.showConfirmDialog(getTab(),
								"Are you sure you want to\noverwrite this file?",
								"", JOptionPane.YES_NO_OPTION,
								JOptionPane.WARNING_MESSAGE);
						
						if (returnVal == JOptionPane.NO_OPTION) {
							return;
						}
					}
					
					TwitterFileWriter.ExportUsersToCSV(tweetDAO.getAllUsers(),
							file);
				}
			}
		}
	}
	
	public TweetDAO getTweetDAO() {
	
		return tweetDAO;
	}
	
	public void setTweetDAO(TweetDAO tweetDAO) {
	
		this.tweetDAO = tweetDAO;
	}
	
	@Override
	public void valueChanged(ListSelectionEvent event) {
	
		int rowIndex = usersTable.getSelectedRow();
		if (rowIndex == -1) {
			return;
		}
		
		Long userId = (Long) usersTable.getModel().getValueAt(rowIndex, 1);
		UserEntity user = userCache.get(userId);
		
		String userImageURL = user.getProfileImageURL();
		try {
			// userImage = ImageIO.read(new URL(userImageURL));
			userImage = ImageHandler.getUserImage(userImageURL);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace());
		}
		
		if (user != null) {
			userTextArea.setText(formatText(user));
		}
	}
	
	private String formatText(UserEntity user) {
	
		String userInfo = "\n\n\nName:  " + user.getRealName()
				+ "\n\n\nScreen Name:  @" + user.getUsername()
				+ "\n\n\nCreated:  ";
		
		if (user.getCreated() != null)
			userInfo += digitFormat.format(user.getCreated());
		
		userInfo += "\n\n\nDescription:  " + user.getDescription();
		
		return userInfo;
	}
	
	private String formatPageLabelText() {
		
		Long totalCount = tweetDAO.getUserCount();
		int numOfUsersShowing = this.userCache.size();
		return "Showing " + this.firstResult + " - "
				+ (this.firstResult + numOfUsersShowing - 1) + "  of  "
				+ NumberFormat.getInstance().format(totalCount);
	}
	
}
