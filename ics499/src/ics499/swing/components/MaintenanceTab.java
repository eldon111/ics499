package ics499.swing.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class MaintenanceTab implements ActionListener{
	
	Border raisedbevel = BorderFactory.createRaisedBevelBorder();
	Border loweredbevel = BorderFactory.createLoweredBevelBorder();
	
	private JPanel tab;

	public JPanel getTab() {
		
		JPanel panelButton = new JPanel();
		panelButton.setLayout(new BorderLayout());		
		
		
		JPanel Box = new JPanel();		
		BoxLayout layout = new BoxLayout(Box, BoxLayout.Y_AXIS);
		Box.setLayout(layout);
		
		panelButton.add(Box, BorderLayout.NORTH);
		
		JButton deleteOne = new JButton(" DELETE SPECIFIC USER");
		deleteOne.setPreferredSize(new Dimension(150, 50));
		deleteOne.setFont(new Font("SansSerif", Font.PLAIN, 16));
		//deleteOne.setBorder(raisedbevel);
		deleteOne.addActionListener(this);
		Box.add(deleteOne);		
		
		Box.add(new JSeparator());
		
		JButton deleteTwo = new JButton(" DELETE ALL EXCEPT USER");
		deleteTwo.setPreferredSize(new Dimension(150, 50));
		deleteTwo.setFont(new Font("SansSerif", Font.PLAIN, 16));
		//deleteTwo.setBorder(raisedbevel);
		deleteTwo.addActionListener(this);
		Box.add(deleteTwo);	
		
		Box.add(new JSeparator());
		
		JButton deleteThree = new JButton(" DELETE ALL IN DATABASE");
		deleteThree.setPreferredSize(new Dimension(150, 50));
		deleteThree.setFont(new Font("SansSerif", Font.PLAIN, 16));
		//deleteThree.setBorder(raisedbevel);
		deleteThree.addActionListener(this);
		
		Box.add(deleteThree);		
		
		
		JPanel panelTexBox = new JPanel();
		panelTexBox.setLayout(new BorderLayout());
		
		
		
		JPanel BoxOne = new JPanel();
		BoxLayout layoutOne = new BoxLayout(BoxOne, BoxLayout.Y_AXIS);
		BoxOne.setLayout(layoutOne);
		
		panelTexBox.add(BoxOne, BorderLayout.NORTH);
		
				
		JTextField textboxOne = new JTextField(" Enter User Who's tweets to delete ");
		textboxOne.setPreferredSize(new Dimension(150, 30));
		textboxOne.setForeground(Color.blue);
		textboxOne.setBorder(loweredbevel);
		BoxOne.add(textboxOne);
		
		BoxOne.add(new JSeparator());
		
		JTextField textboxTwo = new JTextField(" Enter User Who's tweets to keep");
		textboxTwo.setPreferredSize(new Dimension(150, 30));	
		textboxTwo.setForeground(Color.blue);
		textboxTwo.setBorder(loweredbevel);
		BoxOne.add(textboxTwo);		
		
		JButton textboxThree = new JButton(" Delete All in Database");
		textboxThree.setBorder(loweredbevel);
				
		
		// Get a TwitterTab instance with our configured scrollpanes
				this.tab = new TabFactory().newMaintenanceTab( panelButton, panelTexBox);

				return this.tab;
			}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		
	}


}
