package ics499.swing.components;

import ics499.filtering.TweetFilterer;
import ics499.filtering.UserFilter;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

public class UsersTableMenu extends JPopupMenu implements ActionListener,
		MouseListener {
	
	private static final long serialVersionUID = -7481556435520512865L;
	
	JMenuItem mViewTweets;
	JMenuItem mTrackUser;
	JMenuItem mUntrackUser;
	
	JTabbedPane pane;
	Component component;
	
	UsersTab usersTab;
	
	TweetsTab tweetsTab;
	StreamingTab streamingTab;
	
	boolean pressed = false;
	
	public UsersTableMenu(UsersTab usersTab) {
	
		this.usersTab = usersTab;
		
		// add menu items
		mViewTweets = new JMenuItem("View User's Tweets");
		mTrackUser = new JMenuItem("Track This User");
		mUntrackUser = new JMenuItem("Untrack This User");
		
		add(mViewTweets);
		add(mTrackUser);
		add(mUntrackUser);
		
		// add listeners to each menu item
		for (Component item : this.getComponents()) {
			if (item instanceof JMenuItem) {
				((JMenuItem) item).addActionListener(this);
			}
		}
	}
	
	public void setTweetsTab(TweetsTab tweetsTab) {
	
		this.tweetsTab = tweetsTab;
	}
	
	public void setStreamingTab(StreamingTab streamingTab) {
	
		this.streamingTab = streamingTab;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	
		// find tabbed pane ancestor (if one exists)
		if (pane == null) {
			component = usersTab.getTab();
			while (component.getParent() != null) {
				if (component.getParent() instanceof JTabbedPane) {
					pane = (JTabbedPane) component.getParent();
					break;
				}
				component = component.getParent();
			}
		}
		
		// User clicked 'View Users's Tweets'
		if (e.getSource() == mViewTweets) {
			
			// switch to Tweets Tab (JPanel)
			pane.setSelectedComponent(tweetsTab.getTab());
			
			// get Filterer ready with userId
			TweetFilterer tf = tweetsTab.getTweetFilterer();
			tf.clearFilters();
			int row = usersTab.getUsersTable().getSelectedRow();
			int col = 1; // User ID column
			long id = (Long) usersTab.getUsersTable().getModel()
					.getValueAt(row, col);
			tf.addFilter(new UserFilter(id));
			
			// show user's tweets
			tweetsTab.refreshTable();
		}
		
		// User clicked 'Track/Untrack User'
		if (e.getSource() == mTrackUser || e.getSource() == mUntrackUser) {
			
			// get user name
			int row = usersTab.getUsersTable().getSelectedRow();
			int col = 2; // User name column
			String name = (String) usersTab.getUsersTable().getModel()
					.getValueAt(row, col);
			col = 5; // tracked column
			boolean isTracked = (Boolean) usersTab.getUsersTable().getModel()
					.getValueAt(row, col);
			
			// change user's tracked status
			streamingTab.saveUserInfo(name, !isTracked);
			
			// update users table view
			usersTab.refreshTable();
		}
	}
	
	private void processClick(MouseEvent e) {
	
		// if (e.isPopupTrigger()) {
		if (SwingUtilities.isRightMouseButton(e) && pressed) {
			
			pressed = false;
			
			JTable usersTable = usersTab.getUsersTable();
			int row = usersTable.rowAtPoint(e.getPoint());
			if (row >= 0) {
				usersTable.setRowSelectionInterval(row, row);
			}
			int col = 5; // tracked column
			boolean isTracked = (Boolean) usersTab.getUsersTable().getModel()
					.getValueAt(row, col);
			
			if (isTracked) {
				mUntrackUser.setVisible(true);
				mTrackUser.setVisible(false);
			} else {
				mUntrackUser.setVisible(false);
				mTrackUser.setVisible(true);
			}
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
	
		pressed = true;
		processClick(e);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
	
		pressed = false;
		processClick(e);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
	
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	
	}
	
}
