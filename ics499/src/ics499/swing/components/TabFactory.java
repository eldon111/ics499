package ics499.swing.components;

import ics499.swing.*;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

/**
 * Factory for Generic TwitterTabs. All tabs in the application will have get
 * their main layout from here
 * 
 * @author Ryan Brooks
 */
public class TabFactory {
	
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;
	public static final String EXPORT_TEXT = "Export to CSV";
	public static final String PREVIOUS_TEXT = "previous 50";
	public static final String NEXT_TEXT = "    next 50    ";
	
	public JPanel newTwitterTab(int orientation, JComponent firstPanel,
			JComponent secondPanel, Double resizeWeight) {
	
		if (!(firstPanel instanceof JPanel)
				&& !(firstPanel instanceof JScrollPane)
				|| !(secondPanel instanceof JPanel)
				&& !(secondPanel instanceof JScrollPane)) {
			
			throw new RuntimeException(
					"Twitter Tabs may only be instantiated with JTable or JScrollPane instances");
		}
		
		// Main Tab to return
		JPanel tab = new JPanel();
		
		// SET TAB PROPERTIES
		tab.setBorder(new LineBorder(new Color(51, 153, 255)));
		tab.setLayout(new BorderLayout(0, 0));
		
		// SET SPLITPANE PROPERTIES
		JSplitPane splitPane;
		if (orientation == HORIZONTAL) {
			splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
			splitPane.setResizeWeight(resizeWeight);
			splitPane.setTopComponent(firstPanel);
			splitPane.setBottomComponent(secondPanel);
			
		} else {
			splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
			splitPane.setResizeWeight(resizeWeight);
			splitPane.setLeftComponent(firstPanel);
			splitPane.setRightComponent(secondPanel);
		}
		
		tab.add(splitPane);
		
		// padding on East and West
		Component rigidAreaEast = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaEast, BorderLayout.EAST);
		
		Component rigidAreaWest = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaWest, BorderLayout.WEST);
		
		// North Horizontal Box to hold export button
		Box horizontalBoxNorth = Box.createHorizontalBox();
		tab.add(horizontalBoxNorth, BorderLayout.NORTH);
		
		Component rigidAreaNorthBox = Box
				.createRigidArea(new Dimension(10, 20));
		horizontalBoxNorth.add(rigidAreaNorthBox);
		
		Component horizontalGlueNorthBox = Box.createHorizontalGlue();
		horizontalBoxNorth.add(horizontalGlueNorthBox);
		
		JButton exportButton = new JButton(EXPORT_TEXT);
		exportButton.setIcon(new ImageIcon(ApplicationWindow.class
				.getResource("images/exportSmall.png")));
		horizontalBoxNorth.add(exportButton);
		
		Component rigidAreaNorthBox_2 = Box.createRigidArea(new Dimension(10,
				20));
		horizontalBoxNorth.add(rigidAreaNorthBox_2);
		
		// south horizontal box to hold pagination buttons
		Box horizontalBoxSouth = Box.createHorizontalBox();
		horizontalBoxSouth.setPreferredSize(new Dimension(0, 40));
		
		Component rigidAreaSouthBox = Box
				.createRigidArea(new Dimension(13, 20));
		horizontalBoxSouth.add(rigidAreaSouthBox);
		tab.add(horizontalBoxSouth, BorderLayout.SOUTH);
		
		JLabel recordCountLabel = new JLabel("");
		recordCountLabel.setAlignmentY(0.8f);
		recordCountLabel.setVerticalAlignment(SwingConstants.TOP);
		horizontalBoxSouth.add(recordCountLabel);
		
		Component horizontalGlueSouthBox = Box.createHorizontalGlue();
		horizontalBoxSouth.add(horizontalGlueSouthBox);
		
		JButton previousButton = new JButton(PREVIOUS_TEXT);
		horizontalBoxSouth.add(previousButton);
		previousButton.setIcon(new ImageIcon(ApplicationWindow.class
				.getResource("images/previous3.png")));
		
		JButton nextButton = new JButton(NEXT_TEXT);
		horizontalBoxSouth.add(nextButton);
		nextButton.setIcon(new ImageIcon(ApplicationWindow.class
				.getResource("images/next3.png")));
		
		Component rigidAreaSouthBox_2 = Box.createRigidArea(new Dimension(10,
				20));
		horizontalBoxSouth.add(rigidAreaSouthBox_2);
		
		return tab;
	}
	
	public JPanel newUserTab(int orientation, JComponent firstPanel,
			JComponent secondPanel, Double resizeWeight) {
	
		if (!(firstPanel instanceof JPanel)
				&& !(firstPanel instanceof JScrollPane)
				|| !(secondPanel instanceof JPanel)
				&& !(secondPanel instanceof JScrollPane)) {
			
			throw new RuntimeException(
					"Twitter Tabs may only be instantiated with JTable or JScrollPane instances");
		}
		
		// Main Tab to return
		JPanel tab = new JPanel();
		
		// SET TAB PROPERTIES
		tab.setBorder(new LineBorder(new Color(51, 153, 255)));
		tab.setLayout(new BorderLayout(0, 0));
		
		// SET SPLITPANE PROPERTIES
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(resizeWeight);
		
		if (orientation == HORIZONTAL) {
			splitPane.setTopComponent(firstPanel);
			splitPane.setBottomComponent(secondPanel);
			
		} else {
			splitPane.setLeftComponent(firstPanel);
			splitPane.setRightComponent(secondPanel);
		}
		
		tab.add(splitPane);
		
		// padding on East and West
		Component rigidAreaEast = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaEast, BorderLayout.EAST);
		
		Component rigidAreaWest = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaWest, BorderLayout.WEST);
		
		// North Horizontal Box to hold export button
		Box horizontalBoxNorth = Box.createHorizontalBox();
		tab.add(horizontalBoxNorth, BorderLayout.NORTH);
		
		Component rigidAreaNorthBox = Box
				.createRigidArea(new Dimension(10, 20));
		horizontalBoxNorth.add(rigidAreaNorthBox);
		
		Component horizontalGlueNorthBox = Box.createHorizontalGlue();
		horizontalBoxNorth.add(horizontalGlueNorthBox);
		
		JButton exportButton = new JButton(EXPORT_TEXT);
		exportButton.setIcon(new ImageIcon(ApplicationWindow.class
				.getResource("images/exportSmall.png")));
		horizontalBoxNorth.add(exportButton);
		
		Component rigidAreaNorthBox_2 = Box.createRigidArea(new Dimension(10,
				20));
		horizontalBoxNorth.add(rigidAreaNorthBox_2);
		
		// south horizontal box to hold pagination buttons
		Box horizontalBoxSouth = Box.createHorizontalBox();
		horizontalBoxSouth.setPreferredSize(new Dimension(0, 40));
		
		Component rigidAreaSouthBox = Box
				.createRigidArea(new Dimension(13, 20));
		horizontalBoxSouth.add(rigidAreaSouthBox);
		tab.add(horizontalBoxSouth, BorderLayout.SOUTH);
		
		JLabel recordCountLabel = new JLabel("Showing 1 - 50  of  128");
		recordCountLabel.setAlignmentY(0.8f);
		recordCountLabel.setVerticalAlignment(SwingConstants.TOP);
		horizontalBoxSouth.add(recordCountLabel);
		
		Component horizontalGlueSouthBox = Box.createHorizontalGlue();
		horizontalBoxSouth.add(horizontalGlueSouthBox);
		
		JButton previousButton = new JButton(PREVIOUS_TEXT);
		horizontalBoxSouth.add(previousButton);
		previousButton.setIcon(new ImageIcon(ApplicationWindow.class
				.getResource("images/previous3.png")));
		
		JButton nextButton = new JButton(NEXT_TEXT);
		horizontalBoxSouth.add(nextButton);
		nextButton.setIcon(new ImageIcon(ApplicationWindow.class
				.getResource("images/next3.png")));
		
		Component rigidAreaSouthBox_2 = Box.createRigidArea(new Dimension(10,
				20));
		horizontalBoxSouth.add(rigidAreaSouthBox_2);
		
		return tab;
	}
	
	public JPanel newStreamingTab(int orientation, JComponent firstPanel,
			JComponent secondPanel, Double resizeWeight) {
	
		if (!(firstPanel instanceof JPanel)
				&& !(firstPanel instanceof JScrollPane)
				|| !(secondPanel instanceof JPanel)
				&& !(secondPanel instanceof JScrollPane)) {
			
			throw new RuntimeException(
					"Twitter Tabs may only be instantiated with JTable or JScrollPane instances");
		}
		
		// Main Tab to return
		JPanel tab = new JPanel();
		
		// SET TAB PROPERTIES
		tab.setBorder(new LineBorder(new Color(51, 153, 255)));
		tab.setLayout(new BorderLayout(0, 0));
		
		// SET SPLITPANE PROPERTIES
		JSplitPane splitPane = new JSplitPane(HORIZONTAL);
		splitPane.setResizeWeight(resizeWeight);
		
		if (orientation == HORIZONTAL) {
			splitPane.setTopComponent(firstPanel);
			splitPane.setBottomComponent(secondPanel);
			
		} else {
			splitPane.setLeftComponent(firstPanel);
			splitPane.setRightComponent(secondPanel);
		}
		
		tab.add(splitPane);
		
		// padding on East and West
		Component rigidAreaEast = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaEast, BorderLayout.EAST);
		
		Component rigidAreaWest = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaWest, BorderLayout.WEST);
		
		return tab;
	}
	
	public JPanel newTrendsTab(JComponent firstPanel, JComponent secondPanel,JButton refresh) {
	
		if (!(firstPanel instanceof JPanel)
				&& !(firstPanel instanceof JScrollPane)
				|| !(secondPanel instanceof JPanel)
				&& !(secondPanel instanceof JScrollPane)) {
			
			throw new RuntimeException(
					"Twitter Tabs may only be instantiated with JTable or JScrollPane instances");
		}
		
		// Main Tab to return
		JPanel tab = new JPanel();
		
		// SET TAB PROPERTIES
		tab.setBorder(new LineBorder(new Color(51, 153, 255)));
		tab.setLayout(new BorderLayout(0, 0));
		
		// SET SPLITPANE PROPERTIES
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		splitPane.setLeftComponent(firstPanel);
		splitPane.setRightComponent(secondPanel);
		
		tab.add(splitPane);
		
		// padding on East and West
		Component rigidAreaEast = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaEast, BorderLayout.EAST);
		
		Component rigidAreaWest = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaWest, BorderLayout.WEST);
		
		// North Horizontal Box to hold export button
		Box horizontalBoxNorth = Box.createHorizontalBox();
		tab.add(horizontalBoxNorth, BorderLayout.NORTH);
		
		Component rigidAreaNorthBox = Box
				.createRigidArea(new Dimension(10, 20));
		horizontalBoxNorth.add(rigidAreaNorthBox);
		
		Component horizontalGlueNorthBox = Box.createHorizontalGlue();
		horizontalBoxNorth.add(horizontalGlueNorthBox);
		
		Component rigidAreaNorthBox_2 = Box.createRigidArea(new Dimension(10,
				20));
		horizontalBoxNorth.add(rigidAreaNorthBox_2);
		
		// south horizontal box to hold pagination buttons
		Box horizontalBoxSouth = Box.createHorizontalBox();
		horizontalBoxSouth.setPreferredSize(new Dimension(0, 40));
		
		Component rigidAreaSouthBox = Box
				.createRigidArea(new Dimension(13, 20));
		horizontalBoxSouth.add(rigidAreaSouthBox);
		tab.add(horizontalBoxSouth, BorderLayout.SOUTH);
		
		Component horizontalGlueSouthBox = Box.createHorizontalGlue();
		horizontalBoxSouth.add(horizontalGlueSouthBox);
		horizontalBoxSouth.add(refresh);
		Component rigidAreaSouthBox_2 = Box.createRigidArea(new Dimension(10,
				20));
		horizontalBoxSouth.add(rigidAreaSouthBox_2);
		
		
		return tab;
	}
	
	public JPanel newSearchTab(JComponent firstPanel, JButton enter,
			JTextField textBox) {
	
		if (!(firstPanel instanceof JPanel)
				&& !(firstPanel instanceof JScrollPane)) {
			
			throw new RuntimeException(
					"Twitter Tabs may only be instantiated with JTable or JScrollPane instances");
		}
		
		// Main Tab to return
		JPanel tab = new JPanel();
		
		// SET TAB PROPERTIES
		tab.setBorder(new LineBorder(new Color(51, 153, 255)));
		tab.setLayout(new BorderLayout(0, 0));
		
		tab.add(firstPanel);
		
		// padding on East and West
		Component rigidAreaEast = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaEast, BorderLayout.EAST);
		
		Component rigidAreaWest = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaWest, BorderLayout.WEST);
		
		// North Horizontal Box to hold export button
		Box horizontalBoxNorth = Box.createHorizontalBox();
		tab.add(horizontalBoxNorth, BorderLayout.NORTH);
		
		Component rigidAreaNorthBox = Box
				.createRigidArea(new Dimension(10, 25));
		horizontalBoxNorth.add(rigidAreaNorthBox);
		
		Component horizontalGlueNorthBox = Box.createHorizontalGlue();
		horizontalBoxNorth.add(horizontalGlueNorthBox);
		
		horizontalBoxNorth.add(textBox);
		horizontalBoxNorth.add(enter);
		
		Component rigidAreaNorthBox_2 = Box.createRigidArea(new Dimension(650,
				30));
		horizontalBoxNorth.add(rigidAreaNorthBox_2);
		
		// south horizontal box to hold pagination buttons
		Box horizontalBoxSouth = Box.createHorizontalBox();
		horizontalBoxSouth.setPreferredSize(new Dimension(0, 40));
		
		Component rigidAreaSouthBox = Box
				.createRigidArea(new Dimension(13, 20));
		horizontalBoxSouth.add(rigidAreaSouthBox);
		tab.add(horizontalBoxSouth, BorderLayout.SOUTH);
		
		Component horizontalGlueSouthBox = Box.createHorizontalGlue();
		horizontalBoxSouth.add(horizontalGlueSouthBox);
		
		Component rigidAreaSouthBox_2 = Box.createRigidArea(new Dimension(10,
				20));
		horizontalBoxSouth.add(rigidAreaSouthBox_2);
		
		return tab;
	}
	
	public JPanel newLoginTab(JLabel enter) {
		
		
		// Main Tab to return
		JPanel tab = new JPanel();
		
		// SET TAB PROPERTIES
		tab.setBorder(new LineBorder(new Color(51, 153, 255)));
		tab.setLayout(new BorderLayout(0,0));
		tab.add(enter,BorderLayout.CENTER);				
		
		
		return tab;
	}
	
	public JPanel newMaintenanceTab(JComponent firstPanel, JComponent secondPanel) {
		
		if (!(firstPanel instanceof JPanel)
				&& !(firstPanel instanceof JScrollPane)
				|| !(secondPanel instanceof JPanel)
				&& !(secondPanel instanceof JScrollPane)) {
			
			throw new RuntimeException(
					"Twitter Tabs may only be instantiated with JTable or JScrollPane instances");
		}
		
		// Main Tab to return
		JPanel tab = new JPanel();
		
		// SET TAB PROPERTIES
		tab.setBorder(new LineBorder(new Color(51, 153, 255)));
		tab.setLayout(new BorderLayout(0, 0));
		
		// SET SPLITPANE PROPERTIES
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		splitPane.setLeftComponent(firstPanel);
		splitPane.setRightComponent(secondPanel);
		
		tab.add(splitPane);
		
		// padding on East and West
		Component rigidAreaEast = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaEast, BorderLayout.EAST);
		
		Component rigidAreaWest = Box.createRigidArea(new Dimension(8, 20));
		tab.add(rigidAreaWest, BorderLayout.WEST);
		
		// North Horizontal Box to hold export button
		Box horizontalBoxNorth = Box.createHorizontalBox();
		tab.add(horizontalBoxNorth, BorderLayout.NORTH);
		
		Component rigidAreaNorthBox = Box
				.createRigidArea(new Dimension(10, 20));
		horizontalBoxNorth.add(rigidAreaNorthBox);
		
		Component horizontalGlueNorthBox = Box.createHorizontalGlue();
		horizontalBoxNorth.add(horizontalGlueNorthBox);
		
		Component rigidAreaNorthBox_2 = Box.createRigidArea(new Dimension(10,
				20));
		horizontalBoxNorth.add(rigidAreaNorthBox_2);
		
		// south horizontal box to hold pagination buttons
		Box horizontalBoxSouth = Box.createHorizontalBox();
		horizontalBoxSouth.setPreferredSize(new Dimension(0, 40));
		
		Component rigidAreaSouthBox = Box
				.createRigidArea(new Dimension(13, 20));
		horizontalBoxSouth.add(rigidAreaSouthBox);
		tab.add(horizontalBoxSouth, BorderLayout.SOUTH);
		
		Component horizontalGlueSouthBox = Box.createHorizontalGlue();
		horizontalBoxSouth.add(horizontalGlueSouthBox);
		Component rigidAreaSouthBox_2 = Box.createRigidArea(new Dimension(10,
				20));
		horizontalBoxSouth.add(rigidAreaSouthBox_2);
		
		
		return tab;
	}
	
}
