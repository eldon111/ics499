package ics499.swing.components;

import ics499.dataaccess.*;
import ics499.streamingApi.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

/**
 * Representation of the Streaming tab.  Allows the User to enter keywords or
 * twitter user names to be tracked.  Displays what is being tracked in jtables
 * 
 * @author Andre Thibodeau
 */




public class StreamingTab {
	
	private JPanel tab;
	private TweetDAO tweetDAO;
	private JComboBox optionList;
	private JComboBox addorDelete;
	private JTextField textBox;
	private JButton startStream;
	private JButton endStream;
	private Streaming streaming;
	private AbstractStreamingTable keywords;
	private AbstractStreamingTable trackedUsersTable;
	private JRadioButton userNameButton;
    private JRadioButton keywordsButton;
	
	
	public TweetDAO getTweetDAO() {
	
		return tweetDAO;
	}
	
	public void setTweetDAO(TweetDAO tweetDAO) {
	
		this.tweetDAO = tweetDAO;
	}
	
	public StreamingTab() {
	
	}
	
		
	public JPanel getTab() {
	
		if (this.tab != null) {
			return this.tab;
		}
		
		JPanel panel = new JPanel();
		
		String[] optionsAddorDelete = { "Track", "Untrack" };
		addorDelete = new JComboBox(optionsAddorDelete);
		addorDelete.setSelectedIndex(0);
		
		panel.add(addorDelete);
		
		String[] options = { "User Name", "Keyword" };
		optionList = new JComboBox(options);
		optionList.setSelectedIndex(0);
		
		panel.add(optionList);
		
		textBox = new JTextField("Enter your Keyword or UserName");
		textBox.setForeground(Color.blue);
		textBox.setColumns(20);
		panel.add(textBox);
		
		JButton enter = new JButton("Enter");
		enter.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			
				enterEntry();
			}
		});		
		 
		panel.add(enter);
		
		startStream = new JButton("Start Stream");
		startStream.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			
				startStream();
			}
		});
		
		panel.add(startStream);
		
		endStream = new JButton("End Stream");
		endStream.setEnabled(false);
		endStream.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			
				endStream();
			}
		});
		
		panel.add(endStream);
		JPanel panel2 = new JPanel();
		
		keywords = new KeywordTable(tweetDAO);
		panel2.add(keywords);
		this.trackedUsersTable = new TrackedUserTable(tweetDAO);
		panel2.add(trackedUsersTable);
		
		userNameButton = new JRadioButton("User Names");
		keywordsButton = new JRadioButton("Keywords");
		keywordsButton.setSelected(true);
		ButtonGroup group = new ButtonGroup();
		group.add(keywordsButton);
		group.add(userNameButton);
		JPanel radioButtons = new JPanel();
		radioButtons.add(userNameButton);
		radioButtons.add(keywordsButton);
		
		JButton openfile = new JButton("Open File");
        openfile.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent e) {
                openFile();
            }
        });
		
        //radioButtons.add(openfile);
        
        panel2.add(radioButtons);
        panel2.add(openfile);
		this.tab = new TabFactory().newStreamingTab(TabFactory.HORIZONTAL,
				panel, panel2, 0d);
		return tab;
	}
	
	private void startStream() {
	
		startStream.setEnabled(false);
		endStream.setEnabled(true);
		streaming = new Streaming(tweetDAO);
		streaming.StartStream();
		
	}
	
	private void endStream() {
	
		startStream.setEnabled(true);
		endStream.setEnabled(false);
		streaming.endStreaming();
	}
	
	private void enterEntry() {
	
		if (textBox.getText() == null) {
			return;
		}
		
		String addorDeleteChoice = (String) this.addorDelete.getSelectedItem();
		String trackedChoice = (String) optionList.getSelectedItem();
		if (addorDeleteChoice.equals("Track")) {
			if (trackedChoice.equals("Keyword")) {
				saveKeywordInfo(textBox.getText(), true);
			} else if (trackedChoice.equals("User Name")) {
				saveUserInfo(textBox.getText(), true);
			} else {
				System.out
						.println("this else should never happen in the entryEntry 1.0");
			}
		} else if (addorDeleteChoice.equals("Untrack")) {
			if (trackedChoice.equals("Keyword")) {
				saveKeywordInfo(textBox.getText(), false);
			} else if (trackedChoice.equals("User Name")) {
				saveUserInfo(textBox.getText(), false);
			} else {
				System.out
						.println("this else should never happen in the entryEntry 2.0");
			}
		}
	}

	public void saveUserInfo(String name, boolean isTracked) {   
	    boolean isUser = tweetDAO.saveUser(name, isTracked);
	    
	    if (!isUser) {
	        return;
	    }
	    
	    if (isTracked) {
	        this.trackedUsersTable.newTracked(name);
	    } else {
	        this.trackedUsersTable.setToFalse(name);
	    }	  	    
	}
	
	public void saveKeywordInfo(String keyword, boolean isTracked) {	
	    tweetDAO.saveKeyword(keyword, isTracked);
		if (isTracked) {
			keywords.newTracked(keyword);
		} else {
			this.keywords.setToFalse(keyword);
		}
	}
	
	public void openFile() {      
	    FileOpener file = new FileOpener();
	    ArrayList<String> openedFile = file.openFile();
	    if (keywordsButton.isSelected()){
	        for(String keyword: openedFile) {
	            this.saveKeywordInfo(keyword, true);
	        }
	    } else if (userNameButton.isSelected()) {
	        for(String userName: openedFile) {
                this.saveUserInfo(userName, true);
            } 
	    }	    
	}
}
