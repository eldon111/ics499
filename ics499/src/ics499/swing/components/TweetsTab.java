package ics499.swing.components;

import ics499.dataaccess.ImageHandler;
import ics499.dataaccess.TweetEntity;
import ics499.dataaccess.UserEntity;
import ics499.export.TwitterFileWriter;
import ics499.filtering.RetweetFilter;
import ics499.filtering.TextFilter;
import ics499.filtering.TweetFilterer;
import ics499.swing.tablemodel.ListTableModel;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.JTableHeader;

/**
 * Represents the Applications' Tweets Tab. Note that this class does not extend
 * JPanel, but uses composition over inheritance to represent a tab. A JPanel
 * instance is is accessed and returned via the getTab() method. Also note that
 * the instance is first wrapped in a Twitter Tab returned from the TabFactory.
 * Each tab in the Application should do the same.
 * 
 * @author Ryan Brooks
 */
public class TweetsTab implements ActionListener, ListSelectionListener,
		MouseListener, KeyListener {
	
	private JPanel tab;
	private JTable usersTable;
	private Image userImage;
	
	private TweetFilterer tweetFilterer;
	private int textFilterId;
	
	private Popup popup;
	private int firstResult = 1;
	private int previousFirstResult = -1;
	private static final int PAGING_INCREMENT = 50;
	private JLabel pageLabel;
	
	// Create a file chooser
	private JFileChooser fc;
	
	// dialog to prevent unwanted file overwrite
	private JOptionPane overwriteDialog = new JOptionPane(
			"Are you sure you want to\noverwrite this file?",
			JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
	
	// UNCOMMENT WHEN OTHER FILTERS ARE READY
	
	// private SearchTextField realNameTextField = new SearchTextField();
	// private SearchTextField usernameTextField = new SearchTextField();
	private SearchTextField textTextField = new SearchTextField();
	private SearchTextField retweetTextField = new SearchTextField();
	// private SearchTextField dateTextField = new SearchTextField();
	
	private Map<Long, TweetEntity> tweetCache = new HashMap<Long, TweetEntity>();
	
	// For use in UI
	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	// Make the TextArea uneditable
	@SuppressWarnings("serial")
	private JTextArea tweetTextArea = new JTextArea() {
		
		public void paintComponent(Graphics g) {
		
			super.paintComponent(g);
			if (userImage != null) {
				g.drawImage(userImage, 8, 8, this);
			}
		}
	};
	
	// Indexes for the columnModel, to be used when getting values
	public static final int TWEET_ID_INDEX = 0;
	public static final int REAL_NAME_INDEX = 1;
	public static final int USERNAME_INDEX = 2;
	public static final int TEXT_INDEX = 3;
	public static final int IS_RETWEET_INDEX = 4;
	public static final int DATE_INDEX = 5;
	
	@SuppressWarnings("serial")
	public JPanel getTab() {
	
		// Creation of tab happens here, instead of at construction to prevent
		// eager instantiation when Spring initializes the Application
		
		if (this.tab != null) {
			return this.tab;
		}
		
		// Get Tweets to initialize JTable
		List<TweetEntity> tweets = tweetFilterer.getFilteredData(0,
				PAGING_INCREMENT);
		
		// Initialize tweetCache and create a 2-D List for Table Data Model
		@SuppressWarnings("rawtypes")
		ArrayList<List> rows = new ArrayList<List>();
		for (TweetEntity t : tweets) {
			tweetCache.put(t.getTweetId(), t);
			
			ArrayList<Object> entityData = new ArrayList<Object>();
			entityData.add(t.getTweetId());
			entityData.add(t.getUserId().getRealName());
			entityData.add(t.getUserId().getUsername());
			entityData.add(t.getText());
			entityData.add(t.getIsRetweet());
			entityData.add(dateFormat.format(t.getDate()));
			
			rows.add(entityData);
		}
		
		// List of column names to pass to Table Model
		ArrayList<String> columnNames = new ArrayList<String>();
		columnNames.add(TWEET_ID_INDEX, "Tweet ID");
		columnNames.add(REAL_NAME_INDEX, "Name");
		columnNames.add(USERNAME_INDEX, "User Name");
		columnNames.add(TEXT_INDEX, "Text");
		columnNames.add(IS_RETWEET_INDEX, "Is Retweet");
		columnNames.add(DATE_INDEX, "Date");
		
		// Create Tweets Table
		usersTable = new JTable() {
			
			@Override
			public boolean isCellEditable(int rowIndex, int colIndex) {
			
				return false;
			}
		};
		usersTable.setFont(new Font("SansSerif", Font.PLAIN, 13));
		usersTable.setRowHeight(23);
		usersTable.setShowGrid(false);
		usersTable.setShowVerticalLines(true);
		usersTable.setShowHorizontalLines(true);
		usersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		usersTable.getSelectionModel().addListSelectionListener(this);
		
		// Set Table Model using Tweet data and column names
		ListTableModel dataModel = new ListTableModel(6);
		dataModel.insertRows(0, rows);
		usersTable.setModel(dataModel);
		
		for (int i = 1; i < columnNames.size(); i++) {
			usersTable.getColumnModel().getColumn(i)
					.setHeaderRenderer(new ColumnHeaderRenderer());
			usersTable.getColumnModel().getColumn(i)
					.setHeaderValue(columnNames.get(i));
		}
		
		usersTable.getTableHeader().addMouseListener(this);
		
		this.textTextField.addKeyListener(this);
		this.retweetTextField.addKeyListener(this);
		
		// Set column preferences
		usersTable.getColumnModel().getColumn(1).setPreferredWidth(150);
		usersTable.getColumnModel().getColumn(1).setMaxWidth(150);
		usersTable.getColumnModel().getColumn(2).setPreferredWidth(150);
		usersTable.getColumnModel().getColumn(2).setMaxWidth(150);
		usersTable.getColumnModel().getColumn(3).setPreferredWidth(300);
		usersTable.getColumnModel().getColumn(4).setPreferredWidth(100);
		usersTable.getColumnModel().getColumn(4).setMaxWidth(100);
		usersTable.getColumnModel().getColumn(5).setPreferredWidth(100);
		usersTable.getColumnModel().getColumn(5).setMaxWidth(100);
		
		// Tweet ID column is added, then removed so that the data can stay in
		// the TableModel while the column is removed from view.
		usersTable.getColumnModel().removeColumn(
				usersTable.getColumnModel().getColumn(0));
		
		this.tweetTextArea.setWrapStyleWord(true);
		this.tweetTextArea.setLineWrap(true);
		this.tweetTextArea.setFont(new Font("SansSerif", Font.PLAIN, 16));
		this.tweetTextArea.setEditable(false);
		this.tweetTextArea.setBorder(BorderFactory
				.createEmptyBorder(8, 8, 8, 8));
		
		// Initialize TextArea to show individual Tweet Info
		if (rows.size() > 0) {
			usersTable.changeSelection(0, 0, false, false);
		} else {
			this.tweetTextArea.setText("No Data");
		}
		
		// Put Table and TextArea in a ScrollPane
		JScrollPane tableScrollPane = new JScrollPane(usersTable);
		tableScrollPane.setPreferredSize(new Dimension(480, 100));
		
		JScrollPane textScrollPane = new JScrollPane(tweetTextArea);
		
		// Get a TwitterTab instance with our configured scrollpanes
		this.tab = new TabFactory().newTwitterTab(TabFactory.HORIZONTAL,
				tableScrollPane, textScrollPane, 0.8);
		
		// Add Listeners to tab's buttons (must traverse the component tree)
		Component[] tabComponents = tab.getComponents();
		for (int i = 0; i < tabComponents.length; i++) {
			if (tabComponents[i] instanceof Box) {
				
				Component[] boxComponents = ((Box) tabComponents[i])
						.getComponents();
				for (int j = 0; j < boxComponents.length; j++) {
					if (boxComponents[j] instanceof JButton) {
						
						((JButton) boxComponents[j]).addActionListener(this);
					} else if (boxComponents[j] instanceof JLabel) {
						// Add Listener to JLabel to change text
						this.pageLabel = (JLabel) boxComponents[j];
						String pageLabelText = this.formatPageLabelText();
						this.pageLabel.setText(pageLabelText);
					}
				}
			}
		}
		
		// initialize this here, so it will have correct look and feel
		fc = new JFileChooser();
		
		return this.tab;
		
	}
	
	public void setPage(int page) {
	
		if (page < 0) {
			return;
		}
		
		// index to 1 instead of 0
		this.firstResult = page * PAGING_INCREMENT + 1;
		List<TweetEntity> tweets = tweetFilterer.getFilteredData(
				this.firstResult - 1, PAGING_INCREMENT);
		populateModel(tweets);
		String pageLabelText = this.formatPageLabelText();
		this.pageLabel.setText(pageLabelText);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {
	
		JButton button = (JButton) event.getSource();
		
		// use event queue for UI update actions
		if (TabFactory.PREVIOUS_TEXT.equals(button.getText())) {
			// Get last 50 results from TweetDAO
			// Update JLabel to say correct result set
			
			this.firstResult = this.firstResult - PAGING_INCREMENT;
			
			if (this.firstResult < 1) {
				firstResult = 1;
			}
			
			List<TweetEntity> tweets = tweetFilterer.getFilteredData(
					this.firstResult - 1, PAGING_INCREMENT);
			populateModel(tweets);
			String pageLabelText = this.formatPageLabelText();
			this.pageLabel.setText(pageLabelText);
			
		} else if (TabFactory.NEXT_TEXT.equals(button.getText())) {
			// Get next 50 results from TweetDAO
			// Update JLabel to say correct result set
			
			this.firstResult = this.firstResult + PAGING_INCREMENT;
			
			List<TweetEntity> tweets = tweetFilterer.getFilteredData(
					this.firstResult - 1, PAGING_INCREMENT);
			if (tweets.size() == 0) {
				this.firstResult = this.firstResult - PAGING_INCREMENT;
				return;
			}
			
			populateModel(tweets);
			String pageLabelText = this.formatPageLabelText();
			this.pageLabel.setText(pageLabelText);
			
		} else if (TabFactory.EXPORT_TEXT.equals(button.getText())) {
			
			FileNameExtensionFilter ff = new FileNameExtensionFilter(
					"CSV Files", "csv");
			fc.addChoosableFileFilter(ff);
			fc.setFileFilter(ff);
			fc.setDialogType(JFileChooser.SAVE_DIALOG);
			fc.setMultiSelectionEnabled(false);
			fc.setDialogTitle("Save");
			int returnVal = fc.showSaveDialog(getTab());
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				
				String path = fc.getSelectedFile().getAbsolutePath();
				if (!path.matches(".*\\.csv$")) {
					path += ".csv";
				}
				
				File file = new File(path);
				if (file.exists()) {
					// overwriteDialog.sho
					returnVal = JOptionPane.showConfirmDialog(getTab(),
							"Are you sure you want to\noverwrite this file?",
							"", JOptionPane.YES_NO_OPTION,
							JOptionPane.WARNING_MESSAGE);
					
					if (returnVal == JOptionPane.NO_OPTION) {
						return;
					}
				}
				
				TwitterFileWriter.ExportToCSV(tweetFilterer.getFilteredData(),
						file);
			}
		}
	}
	
	@Override
	public void valueChanged(ListSelectionEvent event) {
	
		int rowIndex = usersTable.getSelectedRow();
		if (rowIndex == -1) {
			return;
		}
		
		Long tweetId = (Long) usersTable.getModel().getValueAt(rowIndex,
				TWEET_ID_INDEX);
		TweetEntity tweet = this.tweetCache.get(tweetId);
		
		String userImageURL = tweet.getUserId().getProfileImageURL();
		try {
			// userImage = ImageIO.read(new URL(userImageURL));
			userImage = ImageHandler.getUserImage(userImageURL);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace());
		}
		
		tweetTextArea.setText(formatText(tweet));
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
	
		if (this.popup != null) {
			this.destroyPopup();
		}
		
		JTableHeader headerComponent = (JTableHeader) e.getSource();
		int viewColumnIndex = headerComponent.columnAtPoint(e.getPoint());
		if (viewColumnIndex == -1) {
			return;
		}

		int modelColumnIndex =
		usersTable.convertColumnIndexToModel(viewColumnIndex);
		
		Rectangle cellRect = usersTable.getCellRect(0, viewColumnIndex, false);
		
		Point p = cellRect.getLocation();
		SwingUtilities.convertPointToScreen(p, headerComponent);
		
		int x = (int) p.getX();
		int y = (int) p.getY() + usersTable.getTableHeader().getHeight();
		
		PopupFactory popupFactory = PopupFactory.getSharedInstance();

		
		SearchTextField popupField = null;
		
		 switch (modelColumnIndex) {
			 case TEXT_INDEX:
				 popupField = this.textTextField;
				 break;
			 case IS_RETWEET_INDEX:
				 popupField = this.retweetTextField;
				 break;
			default:
				System.out.println("ERROR: No Search Field Found");
		}
		
		this.popup = popupFactory.getPopup(usersTable, popupField, x, y);
		this.popup.show();
		popupField.requestFocusInWindow();
		
	}
	
	/**
	 * Processes filter request if the corresponding TextArea contains a filter
	 * string. If it is empty it clears the filter for that column and
	 * repopulates the table.
	 */
	@Override
	public void keyPressed(KeyEvent e) {
	
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			int columnIndex;
			SearchTextField textField;
			
			if (e.getSource().equals(this.textTextField)) {
				columnIndex = usersTable.convertColumnIndexToView(TEXT_INDEX);
				textField = this.textTextField;

			} else if (e.getSource().equals(this.retweetTextField)) {
				columnIndex = usersTable.convertColumnIndexToView(IS_RETWEET_INDEX);
				textField = this.retweetTextField;
			} else {
				return;
			}
			
			
			ColumnHeaderRenderer renderer = (ColumnHeaderRenderer) usersTable
					.getColumnModel().getColumn(columnIndex)
					.getHeaderRenderer();
			ListTableModel model = (ListTableModel) this.usersTable.getModel();
			
			if (model.getRowCount() > 0) {
				model.removeRowRange(0, model.getRowCount() - 1);
			}
			
		
			List<TweetEntity> tweets;
			
			if (textField.getText().isEmpty()) {
				
				// tweetFilterer.removeFilter(this.textFilterId);
				tweetFilterer.clearFilters();
				renderer.setFiltered(false);
				
				if (this.previousFirstResult != -1) {
					this.firstResult = previousFirstResult;
					this.previousFirstResult = -1;
				}
				
				tweets = tweetFilterer.getFilteredData(
						this.firstResult - 1, PAGING_INCREMENT);
				populateModel(tweets);
				
				String labelText = this.formatPageLabelText();
				this.pageLabel.setText(labelText);
				
			} else {
				
				if (textField.equals(this.textTextField)){
					this.textFilterId = tweetFilterer.addFilter(new TextFilter(
							this.textTextField.getText()));
					
				} else {
					tweetFilterer.addFilter( new RetweetFilter( new Boolean( textField.getText() ) ) );
				}
				
				renderer.setFiltered(true);
				
				this.previousFirstResult = this.firstResult;
				this.firstResult = 1;
				
				tweets = tweetFilterer.getFilteredData(0, PAGING_INCREMENT);
				populateModel(tweets);
				
				String labelText = this.formatPageLabelText();
				this.pageLabel.setText(labelText);
			}
		
			this.destroyPopup();
		}
	}
	
	public void refreshTable() {
	
		// repopulate the table with filtered tweets
		List<TweetEntity> tweets;
		tweets = tweetFilterer.getFilteredData(0, PAGING_INCREMENT);
		populateModel(tweets);
		
		// this is to select and show the first result
		try {
			usersTable.setRowSelectionInterval(0, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Allows outside components to request focus on table
	 */
	public void requestTableFocus() {
	
		this.usersTable.requestFocusInWindow();
	}
	
	/**
	 * Allows outside components to hide the current popup box (for when the
	 * user "clicks away" from the search box). Also used internally.
	 */
	public void destroyPopup() {
	
		if (this.popup != null) {
			this.popup.hide();
			this.popup = null;
		}
	}
	
	public Rectangle getHeaderRect() {
	
		Rectangle rect = usersTable.getTableHeader().getBounds();
		return rect;
	}
	
	/**
	 * Populates TableModel with data
	 * 
	 * @param tweets
	 *        a list of Tweets to use as data
	 */
	private void populateModel(List<TweetEntity> tweets) {
	
		tweetCache.clear();
		
		@SuppressWarnings("rawtypes")
		ArrayList<List> rows = new ArrayList<List>();
		for (TweetEntity t : tweets) {
			tweetCache.put(t.getTweetId(), t);
			
			ArrayList<Object> entityData = new ArrayList<Object>();
			entityData.add(t.getTweetId());
			entityData.add(t.getUserId().getRealName());
			entityData.add(t.getUserId().getUsername());
			entityData.add(t.getText());
			entityData.add(t.getIsRetweet());
			entityData.add(dateFormat.format(t.getDate()));
			
			rows.add(entityData);
		}
		
		ListTableModel model = (ListTableModel) usersTable.getModel();
		
		if (model.getRowCount() > 0) {
			model.removeRowRange(0, model.getRowCount() - 1);
		}
		
		model.insertRows(0, rows);
		usersTable.getTableHeader().repaint();
	}
	
	/**
	 * Formats the tweet text for display in textArea. Makes user's username and
	 * real name show alongside picture
	 * 
	 * @param tweet
	 *        the tweet to be displayed
	 * @return the formatted text string
	 */
	private String formatText(TweetEntity tweet) {
	
		UserEntity user = tweet.getUserId();
		return "               " + user.getRealName() + "\n               @"
				+ user.getUsername() + "\n\n\n" + tweet.getText() + "\n\n\n"
				+ dateFormat.format(tweet.getDate());
	}
	
	private String formatPageLabelText() {
	
		Long totalCount = tweetFilterer.getFilteredResultCount();
		int numOfTweetsShowing = this.tweetCache.size();
		return "Showing " + this.firstResult + " - "
				+ (this.firstResult + numOfTweetsShowing - 1) + "  of  "
				+ NumberFormat.getInstance().format(totalCount);
	}
	
	public JTable getUsersTable() {
	
		return usersTable;
	}
	
	private int getTweetCount() {
	
		// TODO GET REAL COUNT FROM DB
		return 128;
	}
	
	// SETTER AND GETTERS
	public TweetFilterer getTweetFilterer() {
	
		return tweetFilterer;
	}
	
	public void setTweetFilterer(TweetFilterer tweetFilterer) {
	
		this.tweetFilterer = tweetFilterer;
	}
	
	// UNUSED OVERIDDEN LISTENER METHODS
	@Override
	public void mouseEntered(MouseEvent e) {
	
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
	
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
	
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
	
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	
	}
}
