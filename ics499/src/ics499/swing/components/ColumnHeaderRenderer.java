
package ics499.swing.components;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.*;



class ColumnHeaderRenderer implements TableCellRenderer {


	private ImageIcon icon;
	private Boolean filtered;

	/**
	 * 
	 */
	public ColumnHeaderRenderer() {
		this.filtered = false;
		System.out.println(getClass().getPackage());
		this.icon = new ImageIcon( getClass().getResource("/ics499/swing/images/downArrow.png"));
		this.icon = new ImageIcon( this.icon.getImage().getScaledInstance( 10, 5, Image.SCALE_SMOOTH  ));
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
			boolean hasFocus, int row, int column) {

		JLabel label = (JLabel) table.getTableHeader().getDefaultRenderer().
				getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		label.setIcon(this.icon);
		label.setIconTextGap(10);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setHorizontalTextPosition( JLabel.LEADING );

		if ( this.filtered ) {
			label.setFont(new Font("Arial", Font.ITALIC, 12));
			label.setForeground(Color.BLUE);
		}

		return label;
	}

	public void setFiltered(Boolean filtered) {
		this.filtered = filtered;
	}
	
	

}