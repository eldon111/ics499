package ics499.swing;

import ics499.swing.components.SearchTab;
import ics499.swing.components.StreamingTab;
import ics499.swing.components.TrendsTab;
import ics499.swing.components.TweetsTab;
import ics499.swing.components.UsersTab;
import ics499.swing.components.TweetsTableMenu;
import ics499.swing.components.UsersTableMenu;
import ics499.swing.components.SearchTabTableMenu;
import ics499.swing.components.TrendsTabTableMenu;


import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.LineBorder;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Main Window for the TwitterWatch Application
 * 
 * @author Ryan Brooks
 */
public class ApplicationWindow implements AWTEventListener, MouseListener {
	
	private JFrame frame;
	private TweetsTab tweetsTab;
	private TrendsTab trendsTab;
	private UsersTab usersTab;
	private StreamingTab streamingTab;
	private SearchTab searchTab;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
	
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
			
				// Start ApplicationContext and initialize Main Window
				ApplicationContext context = new ClassPathXmlApplicationContext(
						"ics499/application-context.xml");
				ApplicationWindow window = (ApplicationWindow) context
						.getBean("applicationWindow");
				window.initialize();
				window.frame.setVisible(true);
				window.tweetsTab.requestTableFocus();
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public ApplicationWindow() {
	
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			try {
				UIManager
						.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
			} catch (Exception e1) {
				System.err
						.println("Could not instantiate Look and Feel for UI");
				e1.printStackTrace();
			}
		}
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
	
		// Create main JFrame
		frame = new JFrame("TwitterWatch");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setBounds(50, 50, (int) screenSize.getWidth() - 150,
				(int) screenSize.getHeight() - 150);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(
				ApplicationWindow.class.getResource("images/twitSmallpng.png")));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Create the JPanel for the logo
		JPanel imagePanel = new JPanel();
		imagePanel.setBorder(new LineBorder(new Color(51, 153, 255), 2));
		imagePanel.setBackground(Color.WHITE);
		imagePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		// Create 2 JLabels to hold logo images
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setIcon(new ImageIcon(ApplicationWindow.class
				.getResource("images/logo7.png")));
		imagePanel.add(lblNewLabel);
		
		// Add logo panel to main JFrame
		frame.getContentPane().add(imagePanel, BorderLayout.NORTH);
		
		// Create main JTabbedPane to hold all tabs, add to frame
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(new LineBorder(new Color(51, 153, 255), 2));
		tabbedPane.addMouseListener(this);
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		Toolkit.getDefaultToolkit().addAWTEventListener(this,
				AWTEvent.MOUSE_EVENT_MASK);
		
		// Create Tabs and add them to JTabbedPane		
		JPanel tweetsTabInstance = tweetsTab.getTab();
		tabbedPane.addTab(" Tweets ", null, tweetsTabInstance, null);
		
		JPanel usersTabInstance = usersTab.getTab();
		tabbedPane.addTab(" Users ", null, usersTabInstance, null);
		
		JPanel trendsTabInstance = trendsTab.getTab();
		tabbedPane.addTab(" Trends ", null, trendsTabInstance, null);
		
		JPanel searchTabInstance = searchTab.getTab();
		tabbedPane.addTab(" Search ", null, searchTabInstance, null);
		
		JPanel streaming = streamingTab.getTab();
		tabbedPane.addTab(" Streaming ", null, streaming, null);
		
		
		// add popup menu to the users tab table
		UsersTableMenu utm = new UsersTableMenu(usersTab);
		usersTab.getUsersTable().setComponentPopupMenu(utm);
		usersTab.getUsersTable().addMouseListener(utm);
		utm.setTweetsTab(tweetsTab);
		utm.setStreamingTab(streamingTab);
		
		// add popup menu to the Search tab table
		SearchTabTableMenu stm = new SearchTabTableMenu(searchTab);
		searchTab.getUsersTable().setComponentPopupMenu(stm);
		searchTab.getUsersTable().addMouseListener(stm);
		stm.setStreamingTab(streamingTab);
		
		// add popup menu to the tweets tab table
		TweetsTableMenu ttm = new TweetsTableMenu(tweetsTab);
		tweetsTab.getUsersTable().setComponentPopupMenu(ttm);
		tweetsTab.getUsersTable().addMouseListener(ttm);
		ttm.setUsersTab(usersTab);
		ttm.setStreamingTab(streamingTab);
		
		// add popup menu to the Trends tab table
		TrendsTabTableMenu ttt = new TrendsTabTableMenu(trendsTab);
		trendsTab.getUsersTable().setComponentPopupMenu(ttt);
		trendsTab.getUsersTableOne().setComponentPopupMenu(ttt);
		trendsTab.getUsersTable().addMouseListener(ttt);
		trendsTab.getUsersTableOne().addMouseListener(ttt);
		ttt.setStreamingTab(streamingTab);
		ttt.setSearchTab(searchTab);
	}
	
	@Override
	public void eventDispatched(AWTEvent event) {
	
		if (event instanceof MouseEvent) {
			MouseEvent e = (MouseEvent) event;
			if (e.getID() == MouseEvent.MOUSE_CLICKED) {
				
				Point clickPoint = e.getPoint();
				if (!tweetsTab.getHeaderRect().contains(clickPoint)) {
					tweetsTab.destroyPopup();
				}
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
	
		tweetsTab.destroyPopup();
	}
	
	public TweetsTab getTweetsTab() {
	
		return tweetsTab;
	}
	
	public void setTweetsTab(TweetsTab tweetsTab) {
	
		this.tweetsTab = tweetsTab;
	}
	
	public TrendsTab getTrendsTab() {
	
		return trendsTab;
	}
	
	public void setTrendsTab(TrendsTab trendsTab) {
	
		this.trendsTab = trendsTab;
	}
	
	public UsersTab getUsersTab() {
	
		return usersTab;
	}
	
	public void setUsersTab(UsersTab usersTab) {
	
		this.usersTab = usersTab;
	}
	
	// UNUSED OVERIDDEN LISTENER METHODS
	@Override
	public void mouseEntered(MouseEvent arg0) {
	
	}
	
	@Override
	public void mouseExited(MouseEvent arg0) {
	
	}
	
	@Override
	public void mousePressed(MouseEvent arg0) {
	
	}
	
	@Override
	public void mouseReleased(MouseEvent arg0) {
	
	}
	
	public StreamingTab getStreamingTab() {
	
		return streamingTab;
	}
	
	public void setStreamingTab(StreamingTab streamingTab) {
	
		this.streamingTab = streamingTab;
	}
	
	public SearchTab getSearchTab() {
	
		return searchTab;
	}
	
	public void setSearchTab(SearchTab searchTab) {
	
		this.searchTab = searchTab;
	}

}
