/**
 * Class to export data to file
 * @author Rie
 */

package ics499.export;

import ics499.dataaccess.TweetEntity;
import ics499.dataaccess.UserEntity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.brsanthu.dataexporter.DataExporter;
import com.brsanthu.dataexporter.output.csv.CsvExportOptions;
import com.brsanthu.dataexporter.output.csv.CsvExporter;

public class TwitterFileWriter {

	private static final char COLUMN_DELIMITER = ',';
	private static final char RECORD_DELIMITER = '\n';

	private static DataExporter exporter;

	private static SessionFactory sessionFactory;

	/**
	 * Export all data in the Tweet table to CSV.
	 * @param fileName
	 */
	public static void ExportAllToCSV(String fileName) {
		Session currentSession = sessionFactory.getCurrentSession();

		Transaction transaction = currentSession.beginTransaction();

		String queryStatement = "CALL SYSCS_UTIL.SYSCS_EXPORT_TABLE (null, 'TWEETS' , '"
				+ fileName + "', null, null, null)";
		Query exportQuery = currentSession.createSQLQuery(queryStatement);
		exportQuery.executeUpdate();

		transaction.commit();
	}

	public static void ExportToCSV(String fileName, String sqlQuery) {
		exporter = new CsvExporter();

		Session currentSession = sessionFactory.getCurrentSession();

		Transaction transaction = currentSession.beginTransaction();

		System.out.println(sqlQuery);

		String queryStatement = "CALL SYSCS_UTIL.SYSCS_EXPORT_QUERY ('"
				+ sqlQuery.replaceAll("'", "''") + "', '" + fileName
				+ "', null, null, null)";
		Query exportQuery = currentSession.createSQLQuery(queryStatement);
		exportQuery.executeUpdate();

		transaction.commit();

	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void setSessionFactory(SessionFactory sessionFactory) {
		TwitterFileWriter.sessionFactory = sessionFactory;
	}

	/**
	 * Exports list of TweetEntities to CSV.
	 * @param tweetList
	 * @param fileName
	 */
	public static void ExportToCSV(List<TweetEntity> tweetList, File file) {

		try {
			FileWriter writer = new FileWriter(file);

			exporter = new CsvExporter(writer);

			CsvExportOptions opts = new CsvExportOptions();
			System.out.println(opts.getDelimiter());
			System.out.println(opts.getLineSeparatorString());
			System.out.println(opts.getNullString());
			System.out.println(opts.getQuote());

			exporter.addColumn("TweetID", "UserId", "UserName", "Text", "IsRetweet", "Date");

			for (int i = 0; i < tweetList.size(); i++) {
				TweetEntity tweet = tweetList.get(i);
				UserEntity user = tweet.getUserId();
				
				
				tweet.setText( tweet.getText().replace("\n", "") );
				tweet.setText( tweet.getText().replace("\r", "") );

				exporter.addRow(tweet.getTweetId(), user.getUserId(), user.getUsername(), tweet.getText(), tweet.getIsRetweet(), tweet.getDate());
			}

			exporter.finishExporting();

			writer.close();
			writer.flush();

		} catch (IOException iox) {
			// TODO: Handle IO Exceptions
		} catch (Exception ex) {
			// TODO: Handle other Exceptions
		}
	}

	public static void ExportUsersToCSV(List<UserEntity> userList, File file) {
		try {
			FileWriter writer = new FileWriter(file);

			exporter = new CsvExporter(writer);

			CsvExportOptions opts = new CsvExportOptions();
			System.out.println(opts.getDelimiter());
			System.out.println(opts.getLineSeparatorString());
			System.out.println(opts.getNullString());
			System.out.println(opts.getQuote());
			
			exporter.addColumn("UserID", "Username", "Realname", "IsTracked", "Description", "CreatedDate");

			for (int i = 0; i < userList.size(); i++) {
				UserEntity user = userList.get(i);

				exporter.addRow(user.getUserId(), user.getUsername(), user.getRealName(), user.getIsTracked(), user.getDescription(),user.getCreated());
			}

			exporter.finishExporting();

			writer.close();
			writer.flush();

		} catch (IOException iox) {
			// TODO: Handle IO Exceptions
		} catch (Exception ex) {
			// TODO: Handle other Exceptions
		}
		
	}
}
