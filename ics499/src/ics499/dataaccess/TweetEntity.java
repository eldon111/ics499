package ics499.dataaccess;

import java.util.Date;

/**
 * Represents a tweet database entity (for testing purposes only)
 * 
 * @author Ryan
 * 
 */
public class TweetEntity {

	private long tweetId;
	private UserEntity userId;
	private String text;
	private Date date;
	private boolean isRetweet;
	private long retweetCount;

	public long getTweetId() {

		return tweetId;
	}

	public void setTweetId(long tweetId) {

		this.tweetId = tweetId;
	}

	public String getText() {

		return text;
	}

	public void setText(String text) {

		this.text = text;
	}

	public Boolean getIsRetweet() {

		return isRetweet;
	}

	public void setIsRetweet(Boolean isRetweet) {

		this.isRetweet = isRetweet;
	}

	public Date getDate() {

		return date;
	}

	public void setDate(Date date) {

		this.date = date;
	}

	public long getRetweetCount() {

		return retweetCount;
	}

	public void setRetweetCount(long retweetCount) {

		this.retweetCount = retweetCount;
	}

	public UserEntity getUserId() {

		return userId;
	}

	public void setUserId(UserEntity userId) {

		this.userId = userId;
	}
}
