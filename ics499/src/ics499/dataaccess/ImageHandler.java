package ics499.dataaccess;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;

public class ImageHandler {
	
	private static final String IMAGE_CACHE_PATH = "cache";
	
	public static BufferedImage getUserImage(String userImageURL) {
	
		URL url;
		File imageFile;
		
		try {
			url = new URL(userImageURL);
			imageFile = new File(IMAGE_CACHE_PATH + url.getPath());
			int extensionStart = imageFile.getPath().lastIndexOf('.');
			String extension = (extensionStart >= 0 ? imageFile.getPath()
					.substring(extensionStart + 1) : "");
			
			// if no extension is listed, might as well try .jpg
			if (extension.equals("")) {
				imageFile = new File(imageFile.getPath() + ".jpg");
				extension = "jpg";
			}
			
			if (imageFile.exists()) {
				return ImageIO.read(imageFile);
			} else {
				imageFile.getParentFile().mkdirs();
				imageFile.createNewFile();
				BufferedImage image = ImageIO.read(url);
				ImageIO.write(image, extension, imageFile);
				return image;
			}
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
