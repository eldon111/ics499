package ics499.dataaccess;

/**
 * Represents a tracked keyword database entry
 * @author Eldon
 *
 */
public class KeywordEntity {

	private String keyword;
	private boolean isTracked;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	public boolean getIsTracked() {
		return isTracked;
	}
	
	public void setIsTracked(boolean isTracked) {
		this.isTracked = isTracked;
	}
}
