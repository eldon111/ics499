package ics499.dataaccess;

import java.io.*;
import java.util.*;

import org.hibernate.*;
import org.hibernate.Query;
import org.hibernate.criterion.*;

import twitter4j.*;

public class TweetDAO {
	
	private SessionFactory sessionFactory;
	
	public void save(Status status) {
	
		UserEntity user = new UserEntity();
		user.setRealName(status.getUser().getName());
		user.setUsername(status.getUser().getScreenName());
		user.setUserId(status.getUser().getId());
		user.setIsTracked(false);
		user.setDescription(status.getUser().getDescription());
		user.setCreated(status.getCreatedAt());
		user.setProfileImageURL(status.getUser().getProfileImageURL()
				.toString());
		
		TweetEntity tweet = new TweetEntity();
		tweet.setTweetId(status.getId());
		tweet.setUserId(user);
		tweet.setText(status.getText());
		tweet.setDate(status.getCreatedAt());
		tweet.setIsRetweet(status.isRetweet());
		tweet.setRetweetCount(status.getRetweetCount());
		
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		currentSession.saveOrUpdate(user);
		currentSession.saveOrUpdate(tweet);
		thisTransaction.commit();
	}
	
	public void save(TweetEntity tweetEntity) {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		currentSession.saveOrUpdate(tweetEntity);
		thisTransaction.commit();
	}
	
	public void save(UserEntity userEntity) {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		currentSession.saveOrUpdate(userEntity);
		thisTransaction.commit();
	}
	
	@SuppressWarnings("unchecked")
	public List<TweetEntity> findAll() {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Query findAllQuery = currentSession.createQuery("from "
				+ TweetEntity.class.getSimpleName());
		List<TweetEntity> results = findAllQuery.list();
		thisTransaction.commit();
		
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<TweetEntity> getTweets(int pageNum, int resultsPerPage) {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Criteria crit = currentSession.createCriteria(TweetEntity.class);
		crit.setFirstResult(pageNum);
		crit.setMaxResults(resultsPerPage);
		crit.addOrder(Order.desc("date"));
		List<TweetEntity> results = crit.list();
		thisTransaction.commit();
		
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserEntity> getAllUsers() {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Query findAllQuery = currentSession.createQuery("from UserEntity");
		List<UserEntity> results = findAllQuery.list();
		thisTransaction.commit();
		
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserEntity> getUsers(int pageNum, int resultsPerPage) {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Query findAllQuery = currentSession.createQuery("from UserEntity");
		findAllQuery.setFirstResult(pageNum);
		findAllQuery.setMaxResults(resultsPerPage);
		List<UserEntity> results = findAllQuery.list();
		thisTransaction.commit();
		
		return results;
	}
	
	public TweetEntity findByPrimaryKey(Serializable id) {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		TweetEntity result = (TweetEntity) currentSession.get(
				TweetEntity.class, id);
		thisTransaction.commit();
		
		return result;
	}
	
	public void save(KeywordEntity keyword) {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		currentSession.saveOrUpdate(keyword);
		thisTransaction.commit();
	}
	
	@SuppressWarnings("unchecked")
	public List<KeywordEntity> findAllTrackedKeywords() {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Query findAllQuery = currentSession.createQuery("from "
				+ KeywordEntity.class.getSimpleName()
				+ " where IsTracked = :true");
		findAllQuery.setParameter("true", true);
		List<KeywordEntity> results = findAllQuery.list();
		thisTransaction.commit();
		
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserEntity> findAllTrackedUsers() {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Query findAllQuery = currentSession
				.createQuery("from " + UserEntity.class.getSimpleName()
						+ " where IsTracked = :true");
		findAllQuery.setParameter("true", true);
		List<UserEntity> results = findAllQuery.list();
		thisTransaction.commit();
		
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserEntity> findUser(String name) {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Query findAllQuery = currentSession.createQuery("from "
				+ UserEntity.class.getSimpleName() + " where Username ='"
				+ name + "'");
		List<UserEntity> results = findAllQuery.list();
		thisTransaction.commit();
		
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<KeywordEntity> findAllKeywords() {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Query findAllQuery = currentSession.createQuery("from "
				+ KeywordEntity.class.getSimpleName());
		List<KeywordEntity> results = findAllQuery.list();
		thisTransaction.commit();
		
		return results;
	}
	
	public Long getUserCount() {
		Session session = sessionFactory.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		Criteria crit = session.createCriteria(UserEntity.class);
		
		Long count = (Long) crit.setProjection(Projections.rowCount())
				.uniqueResult();
		
		transaction.commit();
		
		return count;
	}
	
	public boolean saveUser(String user, boolean isTracked) {
	    Twitter twitter = new TwitterFactory().getInstance();
        try {
            User users = twitter.showUser(user);
            if (users != null) {
                UserEntity newUser = new UserEntity();
                newUser.setUserId(users.getId());
                newUser.setIsTracked(isTracked);
                newUser.setRealName(users.getName());
                newUser.setUsername(users.getScreenName());
                newUser.setDescription(users.getDescription());
                newUser.setProfileImageURL(users.getProfileImageURL()
                        .toString());
                newUser.setCreated(users.getCreatedAt());
                this.save(newUser);                  
            } 
        } catch (TwitterException x) {
            return false;
        }
        return true;
	}
	
	public void saveKeyword(String keyword, boolean isTracked) {
	    KeywordEntity keywordSave = new KeywordEntity();
        keywordSave.setKeyword(keyword.toLowerCase());
        keywordSave.setIsTracked(isTracked);
        this.save(keywordSave);
	}
	
	public SessionFactory getSessionFactory() {
	
		return sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
	
		this.sessionFactory = sessionFactory;
	}
	
}
