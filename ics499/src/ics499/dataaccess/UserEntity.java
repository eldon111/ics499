package ics499.dataaccess;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class UserEntity {

	private long userId;
	private String username;
	private String realName;
	private Set<TweetEntity> tweets;
	private boolean isTracked = false;
	private String profileImageURL;
	private String description;
	private Date created;


	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Set<TweetEntity> getTweets() {
		return tweets;
	}

	public void setTweets(Set<TweetEntity> tweets) {
		this.tweets = tweets;
	}

	public void addTweet(TweetEntity tweet) {
		if (tweets == null)
			tweets = new HashSet<TweetEntity>();
		tweets.add(tweet);
	}

	public boolean getIsTracked() {
		return isTracked;
	}

	public void setIsTracked(boolean isTracked) {
		this.isTracked = isTracked;
	}

	public String getProfileImageURL() {
		return profileImageURL;
	}

	public void setProfileImageURL(String profileImageURL) {
		this.profileImageURL = profileImageURL;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
}
