package ics499;

import ics499.dataaccess.TweetDAO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Trends;
import twitter4j.Tweet;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class TwitterManager {
	
	private Twitter twitter;
	private Query query;
	private String searchStatement;
	
	// injected from context
	private TweetDAO tweetDAO;
	
	public TwitterManager() {
	
		twitter = new TwitterFactory().getInstance();
		
	}
	
	// public Image getProfileImage(UserEntity user) {
	//
	// Image image = null;
	//
	// try {
	// // grab full URL
	// URL url = new URL(user.getProfileImageURL());
	//
	// // setup local file location
	// File file = new File("images/" + user.getUserId() + "."
	// + url.getPath());
	//
	// // grab file from disk if it exists, otherwise from the net
	// if (file.exists()) {
	// InputStream is = new BufferedInputStream(new FileInputStream(
	// file));
	// image = ImageIO.read(is);
	// } else {
	// image = ImageIO.read(url);
	// }
	// } catch (MalformedURLException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	//
	// return image;
	// }
	
	public void setQueryStatement() {
	
		query = new Query(searchStatement);
	}
	
	public void setTweetsPerPage(int tweetsPerPage) {
	
		query.setRpp(tweetsPerPage);
	}
	
	public void setPagesToReturn(int pagesWithTweets) {
	
		query.setPage(pagesWithTweets);
	}
	
	public void setSince(String since) {
	
		query.setSince(since);
	}
	
	public void setUntil(String until) {
	
		query.setUntil(until);
	}
	
	public int querySize() {
	
		QueryResult result = null;
		try {
			result = twitter.search(query);
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		return result.getTweets().size();
	}
	
	public QueryResult queryResult(String searchStatement) {
	
		query = new Query(searchStatement);
		
		// query.setPage(3);
		query.setRpp(100);
		
		QueryResult result = null;
		try {
			result = twitter.search(query);
		} catch (TwitterException e) {
			e.printStackTrace();
			return null;
		}

		return result;
		
	}
	
	public void requestLogin(String username, String password) {
	
		twitter.setOAuthConsumer(username, password);	
		
	}
	
	
	public List<Trends> dailyTrends() {
	
		List<Trends> trendsList = null;
		try {
			trendsList = twitter.getDailyTrends();
		} catch (TwitterException e) {
			
			e.printStackTrace();
		}
		
		return trendsList;
		
	}
	
	public List<Trends> weeklyTrends() {
	
		List<Trends> trendsList = null;
		try {
			trendsList = twitter.getWeeklyTrends();
		} catch (TwitterException e) {
			
			e.printStackTrace();
		}
		return trendsList;
	}
	
	public List<Status> getAllTweetsFrom(String tweeter) {
	
		List<Status> list = new ArrayList<Status>();
		// Twitter twitter = new TwitterFactory().getInstance();
		int numOfTweets = 0;
		int numPerPage = 200; // arbitrary, but apparently capped at 200
		
		Paging paging = new Paging();
		paging.setCount(numPerPage);
		
		try {
			numOfTweets = twitter.showUser(tweeter).getStatusesCount();
			System.out.println(numOfTweets);
			// list = twitter.getUserTimeline(tweeter, paging);
			
			List<Status> tmp;
			while ((tmp = twitter.getUserTimeline(tweeter, paging)).size() > 1) {
				System.out.print("Calls remaining: "
						+ twitter.getRateLimitStatus().getRemainingHits());
				System.out.print(" / "
						+ twitter.getRateLimitStatus().getHourlyLimit());
				System.out.println(" - Reset time: "
						+ twitter.getRateLimitStatus().getResetTime());
				
				if (paging.getMaxId() > 0)
					tmp.remove(0); // get rid of first returned tweet (duplicate
									// because of max_id)
					
				list.addAll(tmp);
				// paging.setMaxId(tmp.get(tmp.size() - 1).getId());
				// paging = new Paging();
				if (tmp.size() > 0)
					paging.setMaxId(tmp.get(tmp.size() - 1).getId());
				// paging.setPage(-1);
			}
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public String searchApi(String queryString, Boolean englishOnly) {
	
		Query query = new Query(queryString);
		
		if (englishOnly) {
			query.setLang("en");
		}
		query.setPage(1);
		query.setRpp(100);
		
		QueryResult result = null;
		String tweetString = "";
		int count = 0;
		
		try {
			while (((result = twitter.search(query)).getTweets().size() > 0)
					&& count < 150) {
				
				List<Tweet> tweets = result.getTweets();
				for (Tweet tweet : tweets) {
					count++;
					tweetString += count + ".  " + tweet.getCreatedAt() + " - "
							+ tweet.getFromUser() + " - " + tweet.getText()
							+ "\n";
				}
				
				query.setPage(query.getPage() + 1);
				
			}
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		
		return tweetString;
	}
	
	public String getTweetsAsString() {
	
		// Test Twitter API
		Twitter twitter = new TwitterFactory().getInstance();
		List<Status> statuses = null;
		
		try {
			statuses = twitter.getPublicTimeline();
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		
		String tweets = "";
		for (Status status : statuses) {
			tweets += status.getUser().getName() + ":  " + status.getText()
					+ "\n";
		}
		
		return tweets;
	}
	
	public void printTweets() throws TwitterException {
	
		// Test Twitter API
		Twitter twitter = new TwitterFactory().getInstance();
		List<Status> statuses = twitter.getPublicTimeline();
		
		System.out.println("Showing 20 recent timeline from twitter.");
		
		for (Status status : statuses) {
			System.out.println(status.getUser().getName() + ":"
					+ status.getText());
			
			tweetDAO.save(status);
		}
	}
	
	/**
	 * Calls the Twitter API to see how close we are to our limit
	 * 
	 * @return The number of hits we have left this hour
	 */
	public int checkAPILimit() {
	
		int hitsLeft = 0;
		Date resetTime = new Date();
		
		try {
			hitsLeft = twitter.getRateLimitStatus().getRemainingHits();
			resetTime = twitter.getRateLimitStatus().getResetTime();
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		
		System.out.print("Hits left: " + hitsLeft);
		System.out.println(" - Reset time: " + resetTime);
		
		return hitsLeft;
	}
	
	public String getSearchStatement() {
	
		return searchStatement;
	}
	
	public void setSearchStatement(String searchStatement) {
	
		this.searchStatement = searchStatement;
	}
	
	public TweetDAO getTweetDAO() {
	
		return tweetDAO;
	}
	
	public void setTweetDAO(TweetDAO tweetDAO) {
	
		this.tweetDAO = tweetDAO;
	}
}
