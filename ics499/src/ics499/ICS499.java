package ics499;

import ics499.dataaccess.*;
import ics499.export.*;
import ics499.filtering.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.List;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import org.springframework.context.*;
import org.springframework.context.support.*;

import twitter4j.*;

public class ICS499 implements ActionListener {
	
	JFrame frame;
	JButton button;
	JButton btnOldTweets;
	JButton btnExport;
	JLabel label;
	JTextField text;
	JTextField text2;
	JTextArea area;
	JScrollPane scroll;
	JCheckBox checkbox;
	JFileChooser fc;
	
	JTextField txtUsername;
	JTextField txtPassword;
	JButton btnLogin;
	
	TwitterManager tm;
	TweetFilterer tf;
	TwitterFileWriter tfw;
	
	/**
	 * @param args
	 * @throws TwitterException
	 * 
	 */
	public static void main(String[] args) {
	
		(new ICS499()).run();
	}
	
	public ICS499() {
	
	}
	
	public void run() {
	
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"ics499/application-context.xml");
		
		tm = (TwitterManager) context.getBean("twitterManager");
		tf = (TweetFilterer) context.getBean("tweetFilterer");
		tfw = (TwitterFileWriter) context.getBean("twitterFileWriter");
		
		fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 500);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new FlowLayout());
		frame.setResizable(false);
		
		btnExport = new JButton("Export to CSV");
		btnExport.addActionListener(this);
		frame.add(btnExport);
		
		button = new JButton("Search API");
		button.addActionListener(this);
		frame.add(button);
		
		text2 = new JTextField("#conan");
		frame.add(text2);
		
		checkbox = new JCheckBox("English only");
		frame.add(checkbox);
		
		btnOldTweets = new JButton("Get Tweet history for: ");
		btnOldTweets.addActionListener(this);
		frame.add(btnOldTweets);
		
		text = new JTextField("nathanfillion");
		frame.add(text);
		
		txtUsername = new JTextField("username");
		frame.add(txtUsername);
		
		txtPassword = new JPasswordField("password");
		frame.add(txtPassword);
		
		btnLogin = new JButton("Login");
		btnLogin.addActionListener(this);
		frame.add(btnLogin);
		
		area = new JTextArea();
		scroll = new JScrollPane(area);
		scroll.setPreferredSize(new Dimension(750, 400));
		frame.add(scroll);
		
		frame.setVisible(true);
		
		try {
			// tm.printTweets();
			
			System.out.println("==================================");
			
			SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
			
			tf.clearFilters();
			// tf.addFilter(new TextFilter("cOnSerVatiVe", false));
			tf.addFilter(new TextFilter("romney", false));
			// tf.addFilter(new TextFilter("deficit and (obama or romney)",
			// false));
			// tf.addFilter(new UserFilter(new long[] { 206822202l, 603279939l
			// }));
			// tf.addFilter(new TextFilter("you", false));
			// tf.addFilter(new DateFilter(ft.parse("2012-07-01")));
			// tf.addFilter(new DateFilter(ft.parse("2012-06-30"), ft
			// .parse("2012-06-14")));
			
			// tf.addFilter(new TextFilter("A \"B B B\" or c (d || e) f",
			// false));
			// tf.addFilter(new TextFilter("A and ((B & C) or D) and E",
			// false));
			
			List<TweetEntity> l = tf.getFilteredData(0, 50);
			
			// System.out.println(tf.getSQLQuery());
			
			for (TweetEntity e : l) {
				System.out.println(e.getDate() + " - "
						+ e.getUserId().getUsername() + " : " + e.getText());
				// System.out.println(e.getUserId().getUserId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	
		if (e.getSource() == btnLogin) {
			tm.requestLogin(txtUsername.getText(), txtPassword.getText());
		}
		
		if (e.getSource() == button) {
			area.setText(tm.searchApi(text2.getText(), checkbox.isSelected()));
			// area.setText(tm.getTweetsAsString());
		}
		
		if (e.getSource() == btnOldTweets) {
			List<Status> list = tm.getAllTweetsFrom(text.getText());
			String s = "";
			int i = 1;
			for (Status status : list) {
				s += i++ + "\t" + status.getCreatedAt().toString() + "\t"
						+ status.getText() + "\n";
			}
			
			area.setText(s);
			// area.setText(tm.getOldTweetsAsString(text.getText(), 1, 200));
		}
		
		if (e.getSource() == btnExport) {
			int returnVal = fc.showOpenDialog(null);
			String filePath = null;
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				filePath = file.getPath();
				// This is where a real application would open the file.
				// log.append("Opening: " + file.getName() + "." + newline);
			} else {
				// log.append("Open command cancelled by user." + newline);
			}
			System.out.println(filePath);
			tfw.ExportAllToCSV(filePath);
			
			// String sqlQuery = tf.getSQLQuery();
			// tfw.ExportToCSV("H:\\test.csv", sqlQuery);
			
			// tfw.ExportAllToCSV("H:\\test.csv");
			
		}
	}
	
}
