package ics499.filtering;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class RetweetFilter extends Filter {

	private boolean getRetweets;

	public RetweetFilter(boolean getRetweets) {
		this.getRetweets = getRetweets;
	}

	@Override
	public void addRestrictionTo(Criteria crit) {
		crit.add(Restrictions.eq("isRetweet", getRetweets));
	}

	@Override
	public String getHQLString() {
		return getSQLString();
	}

	@Override
	public String getSQLString() {
		if (getRetweets) {
			return "IsRetweet = 'true'";
		} else {
			return "IsRetweet = 'false'";
		}
	}

	@Override
	public String toString() {
		return "RetweetFilter: " + (getRetweets ? "Get" : "Don't get")
				+ " retweets";
	}

}
