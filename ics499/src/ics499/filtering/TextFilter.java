package ics499.filtering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

/**
 * This filter accepts a space delimited list of words which will be processed
 * using the SQL OR operator. To get the functionality of the AND operator, use
 * multiple TextFilter instances.
 * 
 * example: To achieve: "(a OR b) and (c OR d OR e)" tf.addFilter(new
 * TextFilter("a b"); tf.addFilter(new TextFilter("c d e");
 * 
 * @author Eldon Mathias
 */
public class TextFilter extends Filter {
	
	private String filterText = "";
	private String[] terms;
	private boolean caseSensitive = false;
	
	// private static final String[] OPERATORS = { "OR", "AND", "NOT", "-" };
	private static final String AND = "&";
	private static final String OR = "|";
	private static final String NOT = "~";
	
	public TextFilter(String filterText) {
	
		this(filterText, false);
	}
	
	public TextFilter(String filterText, boolean caseSensitive) {
	
		this.caseSensitive = caseSensitive;
		
		/*
		 * parse filter text and add wildcards
		 */
		String delims = "[ ]+";
		terms = filterText.split(delims);
		
		List<String> list = new ArrayList<String>();
		Collections.addAll(list, terms);
		
		// insert implied ANDs
		for (int i = 1; i < list.size(); i++) {
			if (!isOperator(list.get(i)) && !isOperator(list.get(i - 1))) {
				list.add(i, AND);
			}
		}
		
		terms = list.toArray(new String[0]);
	}
	
	@Override
	public String getHQLString() {
	
		String s;
		
		if (caseSensitive) {
			s = " text like '%" + filterText + "%'";
		} else {
			s = " lower(text) like '%" + filterText + "%'";
		}
		
		return s;
	}
	
	@Override
	public String getSQLString() {
	
		String query = "(";
		for (String term : terms) {
			if (caseSensitive) {
				query += "Text LIKE '" + term + "' OR ";
			} else {
				query += "LCASE(Text) LIKE '" + term.toLowerCase() + "' OR ";
			}
		}
		query = query.substring(0, query.length() - 4) + ")";
		return query;
	}
	
	@Override
	public String toString() {
	
		return "Text filter: " + filterText;
	}
	
	@Override
	public void addRestrictionTo(Criteria crit) {
	
		// evaluateSearch(convertToPostfix(terms));
		//
		for (String term : convertToPostfix(terms)) {
			System.out.print(term + " . ");
		}
		System.out.println();
		//
		// Disjunction disjunction = Restrictions.disjunction();
		// for (String term : terms) {
		// if (caseSensitive) {
		// disjunction.add(Restrictions.like("text", term));
		// } else {
		// disjunction.add(Restrictions.like("text", term).ignoreCase());
		// }
		// }
		// crit.add(disjunction);
		
		crit.add(evaluateSearch(convertToPostfix(terms)));
	}
	
	private List<String> convertToPostfix(String[] originalTerms) {
	
		String[] terms = originalTerms.clone();
		Stack<String> stack = new Stack<String>();
		List<String> out = new ArrayList<String>();
		boolean inQuote = false;
		String quoteString = "";
		
		for (int i = 0; i < terms.length; i++) {
			String c = terms[i];
			if (inQuote) {
				quoteString += " " + c;
				if (c.matches(".*\"$")) { // found closing quote
					quoteString = quoteString.replaceFirst("\"$", "");
					out.add(quoteString.replaceAll(AND, "").replaceAll("[ ]+",
							" "));
					inQuote = false;
				}
				continue;
			}
			if (isOperator(c)) {
				while (!stack.isEmpty() && !stack.peek().matches("^\\(.*")) {
					if (operatorGreaterOrEqual(stack.peek(), c)) {
						out.add(stack.pop());
					} else {
						break;
					}
				}
				stack.push(convertOp(c));
			} else if (c.matches("^\\(.*")) {
				// add a parenthesis to stack and add bare term to out
				stack.push("(");
				terms[i--] = c.substring(1);
				continue; // we don't want to do anything else after this
			} else if (c.matches("^\".*")) { // gather quoted term into one
				quoteString = c.substring(1);
				inQuote = true;
				continue; // we don't want to do anything else after this
			} else if (c.matches(".*\\)$")) {
				// first add final term to out
				out.add(c.replaceFirst("\\)$", ""));
				while (!stack.isEmpty() && !stack.peek().equals("(")) {
					out.add(stack.pop());
				}
				if (!stack.isEmpty()) {
					stack.pop();
				}
			} else if (isOperand(c)) {
				out.add(c);
			}
		}
		while (!stack.empty()) {
			out.add(stack.pop());
		}
		
		return out;
	}
	
	private String convertOp(String op) {
	
		if (isAnd(op)) {
			return AND;
		} else if (isOr(op)) {
			return OR;
		} else if (isNot(op)) {
			return NOT;
		}
		return "";
	}
	
	private Criterion evaluateSearch(List<String> postFix) {
	
		Stack<Criterion> stack = new Stack<Criterion>();
		
		if (postFix.size() >= 3) {
			
			for (String c : postFix) {
				if (isOperand(c)) {
					if (caseSensitive) {
						stack.push(Restrictions.like("text", "%" + c + "%"));
					} else {
						stack.push(Restrictions.like("text", "%" + c + "%")
								.ignoreCase());
					}
				} else if (isOperator(c)) {
					Criterion op1 = stack.pop();
					Criterion op2 = stack.pop();
					
					if (isAnd(c)) {
						stack.push(Restrictions.conjunction().add(op1).add(op2));
					} else if (isOr(c)) {
						stack.push(Restrictions.disjunction().add(op1).add(op2));
					}
				}
			}
			
			return stack.pop();
		} else if (postFix.size() == 1) {
			// search was only one term and we just return that
			if (caseSensitive) {
				return Restrictions.conjunction().add(
						Restrictions.like("text", "%" + postFix.get(0) + "%"));
			} else {
				return Restrictions.conjunction().add(
						Restrictions.like("text", "%" + postFix.get(0) + "%")
								.ignoreCase());
			}
		} else {
			// return null;
			System.out.println("ERROR - invalid postfix");
		}
		
		return null;
	}
	
	private int getPrecedence(String op) {
	
		int ret = 0;
		if (isOr(op)) {
			ret = 1;
		} else if (isAnd(op)) {
			ret = 2;
		} else if (isNot(op)) {
			ret = 3;
		}
		return ret;
	}
	
	private boolean operatorGreaterOrEqual(String op1, String op2) {
	
		return getPrecedence(op1) >= getPrecedence(op2);
	}
	
	private boolean isOperator(String op) {
	
		return (isAnd(op) || isOr(op) || isNot(op));
	}
	
	private boolean isOperand(String op) {
	
		return (!isOperator(op));
	}
	
	private boolean isAnd(String op) {
	
		if (op.toUpperCase().matches("AND") || op.matches("&&?")) {
			return true;
		}
		
		return false;
	}
	
	private boolean isOr(String op) {
	
		if (op.toUpperCase().matches("OR") || op.matches("\\|\\|?")) {
			return true;
		}
		
		return false;
	}
	
	private boolean isNot(String op) {
	
		if (op.toUpperCase().matches("NOT") || op.matches("[!~-]{1}")) {
			return true;
		}
		
		return false;
	}
}
