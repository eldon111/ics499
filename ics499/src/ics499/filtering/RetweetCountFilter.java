package ics499.filtering;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class RetweetCountFilter extends Filter {

	private int minRetweetCount;
	private int maxRetweetCount;

	/**
	 * When setting the count values, if the max value is set to -1, or it is
	 * smaller than the min value, it will be ignored.
	 * 
	 * @param minRetweetCount
	 * @param maxRetweetCount
	 */
	public RetweetCountFilter(int minRetweetCount, int maxRetweetCount) {

		this.minRetweetCount = minRetweetCount;
		this.maxRetweetCount = maxRetweetCount;
	}

	public RetweetCountFilter(int minRetweetCount) {
		this(minRetweetCount, -1);
	}

	@Override
	public void addRestrictionTo(Criteria crit) {

		crit.add(Restrictions.ge("RETWEETCOUNT", minRetweetCount));
		if (maxRetweetCount >= minRetweetCount) {
			crit.add(Restrictions.le("RETWEETCOUNT", maxRetweetCount));
		}
	}

	@Override
	public String getHQLString() {

		return null;
	}

	@Override
	public String getSQLString() {
		String query = "RetweetCount > " + minRetweetCount;
		if (maxRetweetCount >= minRetweetCount) {
			query += " AND RetweetCount < " + maxRetweetCount;
		}
		return query;
	}

	@Override
	public String toString() {

		String s = "Retweet count filter: at least " + minRetweetCount
				+ " tweets";
		if (maxRetweetCount >= minRetweetCount) {
			s += " and no more than " + maxRetweetCount + " tweets";
		}
		return s;
	}
}
