package ics499.filtering;

import ics499.dataaccess.TweetEntity;
import ics499.dataaccess.UserEntity;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class TweetFilterer {
	
	private Set<Filter> filterList = new HashSet<Filter>();
	
	private SessionFactory sessionFactory;
	
	public TweetFilterer() {
	
	}
	
	/**
	 * Adds a new filter to the list of filters.
	 * 
	 * @param filter
	 * @return The filterId of the new filter
	 */
	public int addFilter(Filter filter) {
	
		if (filter == null)
			return -1;
		
		generateFilterId(filter);
		filterList.add(filter);
		
		return filter.getFilterId();
	}
	
	public void clearFilters() {
	
		filterList.clear();
	}
	
	public Set<Filter> getFilters() {
	
		return filterList;
	}
	
	/**
	 * Removes filter with specified id from the filter list.
	 * 
	 * @param filterId
	 */
	public void removeFilter(int filterId) {
	
		Iterator<Filter> iterator = filterList.iterator();
		
		while (iterator.hasNext()) {
			Filter filter = iterator.next();
			
			if (filter.getFilterId() == filterId) {
				iterator.remove();
			}
		}
	}
	
	private void generateFilterId(Filter filter) {
	
		filter.setFilterId(filter.getHQLString().hashCode());
	}
	
	@SuppressWarnings("unchecked")
	public List<TweetEntity> getFilteredData() {
	
		Session session = sessionFactory.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		
		Criteria crit = session.createCriteria(TweetEntity.class);
		for (Filter filter : filterList) {
			filter.addRestrictionTo(crit);
		}
//		crit.addOrder(Order.desc("date"));
		
		List<TweetEntity> results = crit.list();
		transaction.commit();
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<TweetEntity> getFilteredData(int pageNum, int resultsPerPage) {
	
		Session session = sessionFactory.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		Criteria crit = session.createCriteria(TweetEntity.class);
		for (Filter filter : filterList) {
			filter.addRestrictionTo(crit);
		}
		crit.addOrder(Order.desc("date"));
		crit.setFirstResult(pageNum);
		crit.setMaxResults(resultsPerPage);
		
		List<TweetEntity> results = crit.list();
		transaction.commit();
		return results;
	}
	
	@SuppressWarnings("unchecked")
	public Long getFilteredResultCount() {
	
		Session session = sessionFactory.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		Criteria crit = session.createCriteria(TweetEntity.class);
		for (Filter filter : filterList) {
			filter.addRestrictionTo(crit);
		}
		Long count = (Long) crit.setProjection(Projections.rowCount())
				.uniqueResult();
		
		transaction.commit();
		
		return count;
	}
	
	@SuppressWarnings("unchecked")
	public long getUserId(String username) {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Criteria crit = currentSession.createCriteria(UserEntity.class);
		crit.add(Restrictions.like("username", username));
		crit.setFirstResult(0);
		crit.setMaxResults(1);
		List<UserEntity> results = crit.list();
		thisTransaction.commit();
		
		return results.get(0).getUserId();
	}
	
	public boolean isUserTracked(String username) {
	
		Session currentSession = sessionFactory.getCurrentSession();
		
		Transaction thisTransaction = currentSession.beginTransaction();
		Criteria crit = currentSession.createCriteria(UserEntity.class);
		crit.add(Restrictions.like("username", username));
		crit.setFirstResult(0);
		crit.setMaxResults(1);
		List<UserEntity> results = crit.list();
		thisTransaction.commit();
		
		return results.get(0).getIsTracked();
	}
	
	/**
	 * This method creates a valid SQL query String that is equal in function to
	 * processing the set of Filter objects.
	 * 
	 * @return query
	 */
	public String getSQLQuery() {
	
		String query = "SELECT * FROM Tweets WHERE ";
		
		int numOfProcessedFilters = 0;
		for (Filter f : filterList) {
			if (numOfProcessedFilters > 0) {
				query += " AND ";
			}
			query += f.getSQLString();
			numOfProcessedFilters++;
		}
		
		return query;
	}
	
	public SessionFactory getSessionFactory() {
	
		return sessionFactory;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
	
		this.sessionFactory = sessionFactory;
	}
}
