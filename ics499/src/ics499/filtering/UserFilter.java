package ics499.filtering;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class UserFilter extends Filter {

	private Set<Long> userIds = new HashSet<Long>();

	public UserFilter(long userId) {

		addUser(userId);
	}

	public UserFilter(long[] userIds) {

		for (long id : userIds) {
			addUser(id);
		}
	}

	public void addUser(long userId) {

		userIds.add(userId);
	}

	@Override
	public void addRestrictionTo(Criteria crit) {

		crit.add(Restrictions.in("userId.userId", userIds));
	}

	@Override
	public String getHQLString() {

		return "from Tweets where userId = " + userIds;
	}

	@Override
	public String getSQLString() {
		String query = "UserId IN (";
		for (long id : userIds) {
			query += id + ",";
		}
		query = query.substring(0, query.length() - 1) + ")";
		return query;
	}

	@Override
	public String toString() {

		String s = "Users filter: ";
		for (long id : userIds) {
			s += id + ", ";
		}
		s = s.substring(0, s.length() - 2);
		return s;
	}

}
