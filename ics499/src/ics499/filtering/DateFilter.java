package ics499.filtering;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class DateFilter extends Filter {
	
	private Date startDate;
	private Date endDate;
	
	/*
	 * This is for conversion to the format derby stores timestamps in. This is
	 * needed for constructing queries.
	 */
	private SimpleDateFormat timeFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	
	public DateFilter(Date date) {
	
		this(date, date);
	}
	
	/**
	 * filters for tweets from startDate to endDate, inclusive
	 */
	public DateFilter(Date startDate, Date endDate) {
	
		if (startDate.before(endDate)) {
			this.startDate = startDate;
			this.endDate = endDate;
		} else {
			this.startDate = endDate;
			this.endDate = startDate;
		}
		
		// add a day to the end date so we can get tweets for that date
		// inclusively
		Calendar c = Calendar.getInstance();
		c.setTime(this.endDate);
		c.add(Calendar.DATE, 1);
		this.endDate = c.getTime();
	}
	
	@Override
	public String getHQLString() {
	
		String s = " date between " + timeFormat.format(startDate);
		s += " and " + timeFormat.format(endDate);
		
		return s;
	}
	
	@Override
	public String getSQLString() {
	
		return "Date BETWEEN TIMESTAMP('" + timeFormat.format(startDate)
				+ "') AND TIMESTAMP('" + timeFormat.format(endDate) + "')";
	}
	
	@Override
	public void addRestrictionTo(Criteria crit) {
	
		// crit.add(Restrictions.between("date", timeFormat.format(startDate),
		// timeFormat.format(endDate)));
		crit.add(Restrictions.between("date", startDate, endDate));
	}
	
	@Override
	public String toString() {
	
		return "Date filter: between " + timeFormat.format(startDate) + " and "
				+ timeFormat.format(endDate);
	}
	
}
