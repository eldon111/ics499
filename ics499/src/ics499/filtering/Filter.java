package ics499.filtering;

import org.hibernate.Criteria;

public abstract class Filter {

	protected int filterId = -1;

	public void setFilterId(int filterId) {
		this.filterId = filterId;
	}

	public int getFilterId() {
		return filterId;
	}

	public abstract void addRestrictionTo(Criteria crit);

	public abstract String getHQLString();

	public abstract String getSQLString();

	public abstract String toString();
}
