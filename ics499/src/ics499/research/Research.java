package ics499.research;

import ics499.dataaccess.*;

import java.util.*;

public class Research implements Runnable {
    
    private List<TweetEntity> tweets;
    private int wordCount;
    private int aveWordCount;
    private int aveLenghtOfWords;
    private int charCount;
    private int aveCharCount;
    private int numberOfTweets;
    private TreeMap<String, Integer> wordCountMap = new TreeMap<String, Integer>();
    private TreeMap<Date, Integer> dateMap = new TreeMap<Date, Integer>();
    
    public Research (List<TweetEntity> tweets) {
        this.tweets = tweets;
    }
    
    @Override
    public void run() {
        numberOfTweets = tweets.size();
        InnerResearch research = new InnerResearch();
        research.researchTweets();
        //todo set up export to csv or do a table
    }
    

    private class InnerResearch {
        
        public void researchTweets() {
            wordCount = 0;
            charCount = 0;
            aveWordCount = 0;
            aveLenghtOfWords = 0;
            aveCharCount = 0;
            int lengthOfWords = 0;
            for(TweetEntity tweet: tweets) {
                this.tweetsByDate(tweet.getDate());
                String[] wordCountArray = tweet.getText().split(" !,.:;!@#$%^&*()<>[]{}+=\'\"\b\t");                 
                this.wordCount(wordCountArray);
                
                for (String words: wordCountArray) {
                    lengthOfWords += words.length();
                }
                wordCount += wordCountArray.length;
                String text = tweet.getText().replace(" ", "");
                charCount += text.length();
            }
            
            aveWordCount = wordCount / tweets.size();
            aveLenghtOfWords = lengthOfWords / wordCount;
            aveCharCount = charCount / tweets.size();
        }     
        
        /**
        public int AveWords(List<TweetEntity> tweets) {

            int wordCount = 0;
            for(TweetEntity tweet: tweets) {
                String[] text = tweet.getText().split(" ");
                wordCount += text.length;
            }
            return wordCount / tweets.size();
        }

        public int AveCharCount(List<TweetEntity> tweets) {

            int charCount = 0;
            for(TweetEntity tweet: tweets) {
                String text = tweet.getText().replace(" ", "");
                charCount += text.length();
            }
            return charCount / tweets.size();
        }
        **/
        
        public void wordCount(String[] texts){
                for (String text: texts ) {
                    text = text.toLowerCase();
                    if(text.length() > 0) {
                        if(wordCountMap.get(text) == null) {
                            wordCountMap.put(text, 1);
                        } 
                        else {
                            int value = wordCountMap.get(text).intValue();
                            value++;
                            wordCountMap.put(text, value);
                        }
                }
        }          
            //Set<Map.Entry<String, Integer>> entrySet = wordCountMap.entrySet();
            //for (Map.Entry<String, Integer> entry: entrySet) {
            //    System.out.println(entry.getValue() + entry.getKey());
           // }

        }

        public void tweetsByDate(Date date){
                                       
                if(date != null) {
                    if(dateMap.get(date) == null) {
                        dateMap.put(date, 1);
                    } 
                    else {
                        int value = dateMap.get(date).intValue();
                        value++;
                        dateMap.put(date, value);
                    }  
                }
            

           // Set<Map.Entry<Date, Integer>> entrySet = map.entrySet();
           // for (Map.Entry<Date, Integer> entry: entrySet) {
          //      System.out.println(entry.getValue().toString() + entry.getKey());
          //  }
        }
    }
}
