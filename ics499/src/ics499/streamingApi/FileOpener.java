package ics499.streamingApi;
import java.io.*;
import java.util.*;

import javax.swing.*;

public class FileOpener {

    public FileOpener() {
    }
    
    public ArrayList<String> openFile(){           
        ArrayList<String> stringList = new ArrayList<String>();
        try {
        JFileChooser chooser=new  JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.addChoosableFileFilter(new TxtFileFilter());
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            File f = chooser.getSelectedFile();

            String fileName = f.getName();             
            if (fileName != null) {
                String[] fileNameArray = fileName.split("\\.");
                if (fileNameArray[fileNameArray.length - 1].equalsIgnoreCase("txt")) {
                    BufferedReader br=new BufferedReader(new FileReader(f));
                    String st="";               
                    while((st=br.readLine())!=null){
                        String[] stringArray = st.split(" ");
                        for (String string: stringArray) {
                            stringList.add(string); 
                        }
                    }
                } 
            }
        }
        } catch (Exception e) {
            
        }
        return stringList;
    }
}

