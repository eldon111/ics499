package ics499.streamingApi;

import ics499.dataaccess.*;

import java.util.*;
/**
 * Creates the Tracked user Table for the streaming tab
 * 
 * @author Andre Thibodeau
 */
public class TrackedUserTable extends AbstractStreamingTable {
    
    
    public TrackedUserTable (TweetDAO tweetDAO) {
        this.tweetDAO = tweetDAO;
        accessDAO();
        this.setupJPanel();
    }
 
    private void accessDAO (){  
        nameList = new ArrayList<String>();
        List<UserEntity> keywords = tweetDAO.findAllTrackedUsers();
        name = new String[keywords.size()][2];
        for (int i = 0; i < keywords.size(); i ++) {
            name[i][0] = keywords.get(i).getUsername();
            name[i][1] = "" + keywords.get(i).getIsTracked();
            nameList.add(keywords.get(i).getUsername());
            }
        
        String[] columnNameTemp = {"UserName", "isTracked"};
        columnName = columnNameTemp;
    }  
}
