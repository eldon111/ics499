package ics499.streamingApi;

import ics499.dataaccess.*;

import java.util.*;
/**
 * Creates the Keyword table for the streaming tab
 * 
 * @author Andre Thibodeau
 */
public class KeywordTable extends AbstractStreamingTable{
    
    public KeywordTable (TweetDAO tweetDAO) {        
        this.tweetDAO = tweetDAO;
        accessDAO();
        this.setupJPanel();
    }
 
    private void accessDAO (){  
        nameList = new ArrayList<String>();
        List<KeywordEntity> keywords = tweetDAO.findAllKeywords();
        name = new String[keywords.size()][2];
        for (int i = 0; i < keywords.size(); i ++) {
            name[i][0] = keywords.get(i).getKeyword();
            name[i][1] = "" + keywords.get(i).getIsTracked();
            nameList.add(keywords.get(i).getKeyword());
            }
        
        String[] columnNameTemp = {"Keyword", "isTracked"};
        columnName = columnNameTemp;
    }
}
