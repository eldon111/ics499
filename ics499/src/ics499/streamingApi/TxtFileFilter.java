package ics499.streamingApi;

import java.io.*;

import javax.swing.filechooser.FileFilter;

public class TxtFileFilter extends FileFilter{
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
        String fileName = f.getName();  
        if (fileName != null) {
            String[] fileNameArray = fileName.split("\\.");
            
            if (fileNameArray[fileNameArray.length - 1].equalsIgnoreCase("txt")) {
                    return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public String getDescription() {
        return ".txt";
    }
}

