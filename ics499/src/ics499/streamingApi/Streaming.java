package ics499.streamingApi;

import ics499.dataaccess.*;

import java.io.*;
import java.util.*;

import twitter4j.*;
import twitter4j.auth.*;
/**
 * This class takes the tracked keywords and users from the database
 * and starts a thread that listens to twitter and when those keywords/user 
 * tweets happen, takes them from twitter.com and sends them to the TweetDAO
 * to be stored into the database
 * 
 * @author Andre Thibodeau
 */

public class Streaming {
    private StatusListener listener;
    private FilterQuery filterQuery;
    private TwitterStream twitterStream;
    private TweetDAO tweetDAO;
    
    public Streaming(TweetDAO tweetDAO) {
        this.tweetDAO = tweetDAO;     
    }

    public Streaming(long[] ids) {
        filterQuery = new FilterQuery();      
        filterQuery.follow(ids);
        StartStream();
    }
    
    public Streaming (String[] keyWords) {       
        filterQuery = new FilterQuery();
        filterQuery.track(keyWords);  
        StartStream();
    }
    
    public Streaming (long[] ids, String[] keyWords) {
        filterQuery = new FilterQuery();
        filterQuery.track(keyWords);
        filterQuery.follow(ids);
        StartStream();
    }
       
    private String[] convertToStrings(List<KeywordEntity> keywords) {
        String[] strings = new String[keywords.size()];
        for (int i = 0; i < keywords.size(); i++ ) {
            strings[i] = keywords.get(i).getKeyword();
        }
        
        return strings;
    }
    
    private long[] convertToLongs(List<UserEntity> users) {
        long[] ids = new long[users.size()];
        for (int i = 0; i < users.size(); i++ ) {
            ids[i] = users.get(i).getUserId();
        }
        
        return ids;
    }
    
    
    public void StartStream() {   
            //todo checks for nulls and limits on keywords and users
            filterQuery = new FilterQuery();
            filterQuery.track(this.convertToStrings(tweetDAO.findAllTrackedKeywords()));
            filterQuery.follow(this.convertToLongs(tweetDAO.findAllTrackedUsers()));        
            listener = new Listener();
            twitterStream = new TwitterStreamFactory().getInstance();
            twitterStream.setOAuthConsumer("qnPopimAW5QlVDbQePHwg", "kucxzsSaFRp5hEW3LHr0PG0v70PHqgLa8SYSJl5XY");
            AccessToken accessToken = new AccessToken("511290664-icwOTWh98PYqUCgoSsuYpkr1tmTxuxLJ5Ax72uwv", "LY84Y8qHrzVNtWDMCWu8MxqnmffqYf415gTAkwKlQk");
            twitterStream.setOAuthAccessToken(accessToken);
            twitterStream.addListener(listener);
            twitterStream.filter(filterQuery);
        
    }
    
    public void endStreaming() {
        twitterStream.shutdown();
    }
    
    public TweetDAO getTweetDAO() {
        return tweetDAO;
    }

    public void setTweetDAO(TweetDAO tweetDAO) {
        this.tweetDAO = tweetDAO;
    }
    
    public static void main(String[] args) throws TwitterException, IOException{
       // ApplicationContext context = new ClassPathXmlApplicationContext(
      //  //    "ics499/application-context.xml");     
    // Streaming stream = (Streaming) context.getBean("streaming");
  
        String[] string = {"Obama", "Romney"};
        Streaming stream1 = new Streaming(string);
    }
     
    private class Listener implements StatusListener {

        @Override
        public void onException(Exception arg0) {
        }

        @Override
        public void onDeletionNotice(StatusDeletionNotice arg0) {
        }

        @Override
        public void onScrubGeo(long arg0, long arg1) {
        }

        @Override
        public void onStatus(Status status) {           
            System.out.println(status.getUser().getName() + " : " + status.getText() +status.getCreatedAt());        
            tweetDAO.save(status);           
        }

        @Override
        public void onTrackLimitationNotice(int arg0) {
        }
        
        private void converter(Status status) {
            UserEntity user = new UserEntity();
            user.setRealName(status.getUser().getName());
            user.setUsername(status.getUser().getScreenName());
            user.setUserId(status.getUser().getId());
            TweetEntity tweet = new TweetEntity();
           // tweet.setId(status.getId());
            tweet.setUserId(user);
            tweet.setText(status.getText());
            tweet.setDate(status.getCreatedAt());
            tweet.setIsRetweet(status.isRetweet());
            tweet.setRetweetCount(status.getRetweetCount());     
            tweetDAO.save(tweet);
        }
    }
}
