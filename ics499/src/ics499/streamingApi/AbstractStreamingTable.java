package ics499.streamingApi;

import ics499.dataaccess.*;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;
import javax.swing.table.*;

/**This class is an superclass for the tables for the streaming tab
 * This class contains all the common code for the tables for the keyword tracked
 * and twitter user tracked tables
 * 
 * @author Andre Thibodeau
 */

public abstract class AbstractStreamingTable extends JPanel{
    protected List<String> nameList;
    private DefaultTableModel tableModel;
    protected String[] columnName;
    protected String[][] name;
    protected TweetDAO tweetDAO;
    protected JTable keywordTable;    
    private AbstractStreamingTable thisTable;
    
    public void newTracked (String tracked) {        
        
        for (int i = 0; i < nameList.size(); i++) {
            if (nameList.get(i).equalsIgnoreCase(tracked)) {
                if (this instanceof KeywordTable){
                    tableModel.setValueAt("true", i, 1);
                }                 
                return;       
            }
        }
        
        nameList.add(tracked);
        String[] keywordArray = { tracked, "true" };
        tableModel.addRow(keywordArray);        
    }    
    
    public void setToFalse(String tracked) {
        for (int i = 0; i < nameList.size(); i++) {
            if (nameList.get(i).equalsIgnoreCase(tracked)) {
                if (this instanceof TrackedUserTable  /** columnName[0].equals("UserName")**/) {
                    tableModel.removeRow(i);
                    nameList.remove(i);
                } else if (this instanceof KeywordTable){
                    tableModel.setValueAt("false", i, 1);
                } else {
                    System.out.println("Unreachable else in AbstractStreamingTable 1.0");
                }
                return;
                
            }
        }
        //todo print that this element was not being tracked
    } 
    
    private void isTrueOrFalse(String tracked, boolean isTracked) {
        if(isTracked) {
            this.newTracked(tracked);
        } else {
            this.setToFalse(tracked);
        }
    }
    
    public void setupJPanel() { 
        thisTable = this;
        tableModel = new DefaultTableModel(name, columnName);
        keywordTable = new JTable(tableModel) {
            
            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false;
            }
        };
        
        keywordTable.setFont(new Font("SansSerif", Font.PLAIN, 13));
        keywordTable.setRowHeight(23);
        keywordTable.setShowGrid(false);
        keywordTable.setShowVerticalLines(true);
        keywordTable.setShowHorizontalLines(true);
        keywordTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);      
        keywordTable.addMouseListener( new MouseAdapter()
        {
            public void mouseClicked( MouseEvent e )
            {
                if ( SwingUtilities.isRightMouseButton( e ))
                {                  
                    Point p = e.getPoint();                       
                    int rowNumber = keywordTable.rowAtPoint( p );
                    ListSelectionModel model = keywordTable.getSelectionModel();
                    model.setSelectionInterval( rowNumber, rowNumber );                                    
                    RightClickPopUp menu = new RightClickPopUp(rowNumber);
                    menu.show(e.getComponent(), e.getX(), e.getY());      
                }
            }
        });
        
        this.add(new JScrollPane(keywordTable));
    }

    class RightClickPopUp extends JPopupMenu {
        JMenuItem anItem;
        String keyword;
        boolean isTrue;

        public RightClickPopUp(int rowNumber){
            keyword = (String)keywordTable.getValueAt(rowNumber, 0);
            String isTrueString = (String)keywordTable.getValueAt(rowNumber, 1);
            if (isTrueString.equalsIgnoreCase("true")) {
                anItem = new JMenuItem("false");
                isTrue = false;
            } else {
                anItem = new JMenuItem("true");
                isTrue = true;
            }      

            add(anItem);

            anItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    changeMenu();           
                }
            });
        }

        private void changeMenu() {
            tweetDAO.saveKeyword(keyword, isTrue);
            thisTable.isTrueOrFalse(keyword, isTrue);           
        }
    }
}
